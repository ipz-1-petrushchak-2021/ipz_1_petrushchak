﻿
namespace Repair_As_New
{
    partial class Prices_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.panel48 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.panel49 = new System.Windows.Forms.Panel();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel31 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel32 = new System.Windows.Forms.Panel();
            this.panel53 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.panel54 = new System.Windows.Forms.Panel();
            this.panel55 = new System.Windows.Forms.Panel();
            this.panel56 = new System.Windows.Forms.Panel();
            this.panel57 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.panel23 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel25 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.label24 = new System.Windows.Forms.Label();
            this.panel37 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel33 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.panel62 = new System.Windows.Forms.Panel();
            this.label23 = new System.Windows.Forms.Label();
            this.panel63 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.panel43 = new System.Windows.Forms.Panel();
            this.panel44 = new System.Windows.Forms.Panel();
            this.panel45 = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.panel47 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel59 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.panel60 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel40 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panel38 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.panel51 = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panel61 = new System.Windows.Forms.Panel();
            this.child_panel_first = new System.Windows.Forms.Panel();
            this.panel127 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.child_panel_second = new System.Windows.Forms.Panel();
            this.panel35 = new System.Windows.Forms.Panel();
            this.label25 = new System.Windows.Forms.Label();
            this.panel64 = new System.Windows.Forms.Panel();
            this.panel65 = new System.Windows.Forms.Panel();
            this.label27 = new System.Windows.Forms.Label();
            this.panel66 = new System.Windows.Forms.Panel();
            this.panel67 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.panel68 = new System.Windows.Forms.Panel();
            this.panel69 = new System.Windows.Forms.Panel();
            this.panel70 = new System.Windows.Forms.Panel();
            this.panel71 = new System.Windows.Forms.Panel();
            this.panel72 = new System.Windows.Forms.Panel();
            this.panel73 = new System.Windows.Forms.Panel();
            this.label30 = new System.Windows.Forms.Label();
            this.panel74 = new System.Windows.Forms.Panel();
            this.label34 = new System.Windows.Forms.Label();
            this.panel75 = new System.Windows.Forms.Panel();
            this.panel76 = new System.Windows.Forms.Panel();
            this.label31 = new System.Windows.Forms.Label();
            this.panel77 = new System.Windows.Forms.Panel();
            this.panel78 = new System.Windows.Forms.Panel();
            this.panel79 = new System.Windows.Forms.Panel();
            this.panel80 = new System.Windows.Forms.Panel();
            this.panel81 = new System.Windows.Forms.Panel();
            this.panel82 = new System.Windows.Forms.Panel();
            this.label35 = new System.Windows.Forms.Label();
            this.panel83 = new System.Windows.Forms.Panel();
            this.panel84 = new System.Windows.Forms.Panel();
            this.panel85 = new System.Windows.Forms.Panel();
            this.panel86 = new System.Windows.Forms.Panel();
            this.panel87 = new System.Windows.Forms.Panel();
            this.panel88 = new System.Windows.Forms.Panel();
            this.label37 = new System.Windows.Forms.Label();
            this.panel89 = new System.Windows.Forms.Panel();
            this.panel90 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.panel91 = new System.Windows.Forms.Panel();
            this.panel92 = new System.Windows.Forms.Panel();
            this.panel93 = new System.Windows.Forms.Panel();
            this.label41 = new System.Windows.Forms.Label();
            this.panel94 = new System.Windows.Forms.Panel();
            this.panel95 = new System.Windows.Forms.Panel();
            this.label39 = new System.Windows.Forms.Label();
            this.panel96 = new System.Windows.Forms.Panel();
            this.label42 = new System.Windows.Forms.Label();
            this.panel97 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.panel98 = new System.Windows.Forms.Panel();
            this.panel99 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel100 = new System.Windows.Forms.Panel();
            this.panel101 = new System.Windows.Forms.Panel();
            this.panel102 = new System.Windows.Forms.Panel();
            this.panel103 = new System.Windows.Forms.Panel();
            this.panel104 = new System.Windows.Forms.Panel();
            this.panel105 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.panel106 = new System.Windows.Forms.Panel();
            this.panel107 = new System.Windows.Forms.Panel();
            this.panel108 = new System.Windows.Forms.Panel();
            this.panel109 = new System.Windows.Forms.Panel();
            this.panel110 = new System.Windows.Forms.Panel();
            this.panel111 = new System.Windows.Forms.Panel();
            this.label43 = new System.Windows.Forms.Label();
            this.panel112 = new System.Windows.Forms.Panel();
            this.panel113 = new System.Windows.Forms.Panel();
            this.label38 = new System.Windows.Forms.Label();
            this.panel114 = new System.Windows.Forms.Panel();
            this.panel115 = new System.Windows.Forms.Panel();
            this.label44 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.panel125 = new System.Windows.Forms.Panel();
            this.panel116 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.panel117 = new System.Windows.Forms.Panel();
            this.panel118 = new System.Windows.Forms.Panel();
            this.panel119 = new System.Windows.Forms.Panel();
            this.panel120 = new System.Windows.Forms.Panel();
            this.panel121 = new System.Windows.Forms.Panel();
            this.panel122 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.panel123 = new System.Windows.Forms.Panel();
            this.label46 = new System.Windows.Forms.Label();
            this.panel124 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.roundButton2 = new Repair_As_New.RoundButton();
            this.roundButton1 = new Repair_As_New.RoundButton();
            this.panel126 = new System.Windows.Forms.Panel();
            this.panel4.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel48.SuspendLayout();
            this.panel17.SuspendLayout();
            this.panel18.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel23.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel36.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel52.SuspendLayout();
            this.child_panel_first.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.child_panel_second.SuspendLayout();
            this.panel35.SuspendLayout();
            this.panel65.SuspendLayout();
            this.panel67.SuspendLayout();
            this.panel68.SuspendLayout();
            this.panel69.SuspendLayout();
            this.panel71.SuspendLayout();
            this.panel73.SuspendLayout();
            this.panel74.SuspendLayout();
            this.panel76.SuspendLayout();
            this.panel77.SuspendLayout();
            this.panel78.SuspendLayout();
            this.panel80.SuspendLayout();
            this.panel82.SuspendLayout();
            this.panel83.SuspendLayout();
            this.panel84.SuspendLayout();
            this.panel86.SuspendLayout();
            this.panel88.SuspendLayout();
            this.panel90.SuspendLayout();
            this.panel93.SuspendLayout();
            this.panel95.SuspendLayout();
            this.panel96.SuspendLayout();
            this.panel97.SuspendLayout();
            this.panel99.SuspendLayout();
            this.panel100.SuspendLayout();
            this.panel101.SuspendLayout();
            this.panel103.SuspendLayout();
            this.panel105.SuspendLayout();
            this.panel106.SuspendLayout();
            this.panel107.SuspendLayout();
            this.panel109.SuspendLayout();
            this.panel111.SuspendLayout();
            this.panel113.SuspendLayout();
            this.panel115.SuspendLayout();
            this.panel116.SuspendLayout();
            this.panel117.SuspendLayout();
            this.panel118.SuspendLayout();
            this.panel120.SuspendLayout();
            this.panel122.SuspendLayout();
            this.panel123.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.LightGray;
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(-9, 181);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(674, 31);
            this.panel4.TabIndex = 50;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(10, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(633, 24);
            this.label4.TabIndex = 25;
            this.label4.Text = "1.1.         Комп’ютерна діагностика                                             " +
    "                           200";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Gray;
            this.panel5.Location = new System.Drawing.Point(0, 30);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(674, 31);
            this.panel5.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(269, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 29);
            this.label7.TabIndex = 46;
            this.label7.Text = "Ціни";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.LightGray;
            this.panel14.Controls.Add(this.label6);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Location = new System.Drawing.Point(-9, 241);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(674, 31);
            this.panel14.TabIndex = 52;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(10, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(633, 24);
            this.label6.TabIndex = 30;
            this.label6.Text = "1.3.         Діагностика гальмівної системи                                      " +
    "                      200";
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Gray;
            this.panel15.Location = new System.Drawing.Point(0, 30);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(674, 31);
            this.panel15.TabIndex = 24;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel16.Controls.Add(this.label9);
            this.panel16.ForeColor = System.Drawing.SystemColors.Window;
            this.panel16.Location = new System.Drawing.Point(-9, 301);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(674, 37);
            this.panel16.TabIndex = 54;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(205, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(271, 24);
            this.label9.TabIndex = 24;
            this.label9.Text = "2.Ремонт рульової рейки";
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.LightGray;
            this.panel48.Controls.Add(this.label16);
            this.panel48.Controls.Add(this.panel49);
            this.panel48.Location = new System.Drawing.Point(-9, 495);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(674, 31);
            this.panel48.TabIndex = 60;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(10, 4);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(633, 24);
            this.label16.TabIndex = 25;
            this.label16.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.Gray;
            this.panel49.Location = new System.Drawing.Point(0, 30);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(674, 31);
            this.panel49.TabIndex = 24;
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.SystemColors.Window;
            this.panel17.Controls.Add(this.label11);
            this.panel17.Controls.Add(this.panel18);
            this.panel17.Location = new System.Drawing.Point(-9, 368);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(674, 31);
            this.panel17.TabIndex = 56;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(10, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(633, 24);
            this.label11.TabIndex = 29;
            this.label11.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Gray;
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Controls.Add(this.panel21);
            this.panel18.Location = new System.Drawing.Point(0, 30);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(674, 31);
            this.panel18.TabIndex = 24;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.SystemColors.Window;
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Location = new System.Drawing.Point(0, 30);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(674, 31);
            this.panel19.TabIndex = 26;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Gray;
            this.panel20.Location = new System.Drawing.Point(0, 30);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(674, 31);
            this.panel20.TabIndex = 24;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.Gray;
            this.panel21.Controls.Add(this.panel22);
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(674, 31);
            this.panel21.TabIndex = 25;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.Gray;
            this.panel22.Location = new System.Drawing.Point(0, 30);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(674, 31);
            this.panel22.TabIndex = 24;
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.LightGray;
            this.panel31.Controls.Add(this.label13);
            this.panel31.Controls.Add(this.panel32);
            this.panel31.Location = new System.Drawing.Point(-9, 398);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(674, 31);
            this.panel31.TabIndex = 57;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(10, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(633, 24);
            this.label13.TabIndex = 25;
            this.label13.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.Gray;
            this.panel32.Location = new System.Drawing.Point(0, 30);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(674, 31);
            this.panel32.TabIndex = 24;
            // 
            // panel53
            // 
            this.panel53.BackColor = System.Drawing.SystemColors.Window;
            this.panel53.Controls.Add(this.label19);
            this.panel53.Controls.Add(this.panel54);
            this.panel53.Location = new System.Drawing.Point(-9, 652);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(674, 31);
            this.panel53.TabIndex = 65;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(10, 4);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(633, 24);
            this.label19.TabIndex = 29;
            this.label19.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel54
            // 
            this.panel54.BackColor = System.Drawing.Color.Gray;
            this.panel54.Controls.Add(this.panel55);
            this.panel54.Controls.Add(this.panel57);
            this.panel54.Location = new System.Drawing.Point(0, 30);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(674, 31);
            this.panel54.TabIndex = 24;
            // 
            // panel55
            // 
            this.panel55.BackColor = System.Drawing.SystemColors.Window;
            this.panel55.Controls.Add(this.panel56);
            this.panel55.Location = new System.Drawing.Point(0, 30);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(674, 31);
            this.panel55.TabIndex = 26;
            // 
            // panel56
            // 
            this.panel56.BackColor = System.Drawing.Color.Gray;
            this.panel56.Location = new System.Drawing.Point(0, 30);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(674, 31);
            this.panel56.TabIndex = 24;
            // 
            // panel57
            // 
            this.panel57.BackColor = System.Drawing.Color.Gray;
            this.panel57.Controls.Add(this.panel58);
            this.panel57.Location = new System.Drawing.Point(0, 0);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(674, 31);
            this.panel57.TabIndex = 25;
            // 
            // panel58
            // 
            this.panel58.BackColor = System.Drawing.Color.Gray;
            this.panel58.Location = new System.Drawing.Point(0, 30);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(674, 31);
            this.panel58.TabIndex = 24;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel3.Controls.Add(this.label3);
            this.panel3.ForeColor = System.Drawing.SystemColors.Window;
            this.panel3.Location = new System.Drawing.Point(-9, 144);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(674, 37);
            this.panel3.TabIndex = 49;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(248, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 24);
            this.label3.TabIndex = 24;
            this.label3.Text = "1.Діагностика автомобіля";
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.LightGray;
            this.panel23.Controls.Add(this.label10);
            this.panel23.Controls.Add(this.panel24);
            this.panel23.Location = new System.Drawing.Point(-9, 338);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(674, 31);
            this.panel23.TabIndex = 55;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(10, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(633, 24);
            this.label10.TabIndex = 25;
            this.label10.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.Gray;
            this.panel24.Location = new System.Drawing.Point(0, 30);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(674, 31);
            this.panel24.TabIndex = 24;
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.SystemColors.Window;
            this.panel25.Controls.Add(this.label12);
            this.panel25.Controls.Add(this.panel26);
            this.panel25.Location = new System.Drawing.Point(-9, 428);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(674, 31);
            this.panel25.TabIndex = 58;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(10, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(633, 24);
            this.label12.TabIndex = 29;
            this.label12.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Gray;
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Controls.Add(this.panel29);
            this.panel26.Location = new System.Drawing.Point(0, 30);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(674, 31);
            this.panel26.TabIndex = 24;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.SystemColors.Window;
            this.panel27.Controls.Add(this.panel28);
            this.panel27.Location = new System.Drawing.Point(0, 30);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(674, 31);
            this.panel27.TabIndex = 26;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.Gray;
            this.panel28.Location = new System.Drawing.Point(0, 30);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(674, 31);
            this.panel28.TabIndex = 24;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.Gray;
            this.panel29.Controls.Add(this.panel30);
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(674, 31);
            this.panel29.TabIndex = 25;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.Gray;
            this.panel30.Location = new System.Drawing.Point(0, 30);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(674, 31);
            this.panel30.TabIndex = 24;
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.LightGray;
            this.panel36.Controls.Add(this.label24);
            this.panel36.Controls.Add(this.panel37);
            this.panel36.Location = new System.Drawing.Point(-9, 683);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(674, 31);
            this.panel36.TabIndex = 66;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(10, 4);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(633, 24);
            this.label24.TabIndex = 25;
            this.label24.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.Gray;
            this.panel37.Location = new System.Drawing.Point(0, 30);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(674, 31);
            this.panel37.TabIndex = 24;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Repair_As_New.Properties.Resources._172529_price_usd_tag_icon__2_;
            this.pictureBox3.Location = new System.Drawing.Point(182, 21);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(55, 55);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 47;
            this.pictureBox3.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.ForeColor = System.Drawing.SystemColors.Window;
            this.panel2.Location = new System.Drawing.Point(-9, 101);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(674, 37);
            this.panel2.TabIndex = 48;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(495, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 24);
            this.label2.TabIndex = 23;
            this.label2.Text = "Ціна, грн*";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(46, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 24);
            this.label1.TabIndex = 21;
            this.label1.Text = "Найменування";
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel33.Controls.Add(this.label14);
            this.panel33.ForeColor = System.Drawing.SystemColors.Window;
            this.panel33.Location = new System.Drawing.Point(-9, 458);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(674, 37);
            this.panel33.TabIndex = 59;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(205, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(271, 24);
            this.label14.TabIndex = 24;
            this.label14.Text = "2.Ремонт рульової рейки";
            // 
            // panel62
            // 
            this.panel62.BackColor = System.Drawing.Color.LightGray;
            this.panel62.Controls.Add(this.label23);
            this.panel62.Controls.Add(this.panel63);
            this.panel62.Location = new System.Drawing.Point(-9, 743);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(674, 31);
            this.panel62.TabIndex = 68;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(10, 3);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(633, 24);
            this.label23.TabIndex = 25;
            this.label23.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel63
            // 
            this.panel63.BackColor = System.Drawing.Color.Gray;
            this.panel63.Location = new System.Drawing.Point(0, 30);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(674, 31);
            this.panel63.TabIndex = 24;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel34.Controls.Add(this.label18);
            this.panel34.ForeColor = System.Drawing.SystemColors.Window;
            this.panel34.Location = new System.Drawing.Point(-9, 585);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(674, 37);
            this.panel34.TabIndex = 63;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(205, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(271, 24);
            this.label18.TabIndex = 24;
            this.label18.Text = "2.Ремонт рульової рейки";
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.SystemColors.Window;
            this.panel42.Controls.Add(this.label15);
            this.panel42.Controls.Add(this.panel43);
            this.panel42.Location = new System.Drawing.Point(-9, 525);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(674, 31);
            this.panel42.TabIndex = 61;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(10, 4);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(633, 24);
            this.label15.TabIndex = 29;
            this.label15.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel43
            // 
            this.panel43.BackColor = System.Drawing.Color.Gray;
            this.panel43.Controls.Add(this.panel44);
            this.panel43.Controls.Add(this.panel46);
            this.panel43.Location = new System.Drawing.Point(0, 30);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(674, 31);
            this.panel43.TabIndex = 24;
            // 
            // panel44
            // 
            this.panel44.BackColor = System.Drawing.SystemColors.Window;
            this.panel44.Controls.Add(this.panel45);
            this.panel44.Location = new System.Drawing.Point(0, 30);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(674, 31);
            this.panel44.TabIndex = 26;
            // 
            // panel45
            // 
            this.panel45.BackColor = System.Drawing.Color.Gray;
            this.panel45.Location = new System.Drawing.Point(0, 30);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(674, 31);
            this.panel45.TabIndex = 24;
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.Gray;
            this.panel46.Controls.Add(this.panel47);
            this.panel46.Location = new System.Drawing.Point(0, 0);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(674, 31);
            this.panel46.TabIndex = 25;
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.Color.Gray;
            this.panel47.Location = new System.Drawing.Point(0, 30);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(674, 31);
            this.panel47.TabIndex = 24;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Window;
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Location = new System.Drawing.Point(-9, 211);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(674, 31);
            this.panel6.TabIndex = 51;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(10, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(633, 24);
            this.label5.TabIndex = 29;
            this.label5.Text = "1.2.         Діагностика ходової частини                                         " +
    "                         200";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Gray;
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Location = new System.Drawing.Point(0, 30);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(674, 31);
            this.panel7.TabIndex = 24;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Window;
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Location = new System.Drawing.Point(0, 30);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(674, 31);
            this.panel8.TabIndex = 26;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Gray;
            this.panel9.Location = new System.Drawing.Point(0, 30);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(674, 31);
            this.panel9.TabIndex = 24;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Gray;
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(674, 31);
            this.panel10.TabIndex = 25;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Gray;
            this.panel11.Location = new System.Drawing.Point(0, 30);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(674, 31);
            this.panel11.TabIndex = 24;
            // 
            // panel59
            // 
            this.panel59.BackColor = System.Drawing.Color.LightGray;
            this.panel59.Controls.Add(this.label20);
            this.panel59.Controls.Add(this.panel60);
            this.panel59.Location = new System.Drawing.Point(-9, 622);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(674, 31);
            this.panel59.TabIndex = 64;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(10, 4);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(633, 24);
            this.label20.TabIndex = 25;
            this.label20.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel60
            // 
            this.panel60.BackColor = System.Drawing.Color.Gray;
            this.panel60.Location = new System.Drawing.Point(0, 30);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(674, 31);
            this.panel60.TabIndex = 24;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.Window;
            this.panel12.Controls.Add(this.label8);
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Location = new System.Drawing.Point(-9, 271);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(674, 31);
            this.panel12.TabIndex = 53;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(10, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(633, 24);
            this.label8.TabIndex = 30;
            this.label8.Text = "1.1.         Комп’ютерна діагностика                                             " +
    "                           200";
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Gray;
            this.panel13.Location = new System.Drawing.Point(0, 30);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(674, 31);
            this.panel13.TabIndex = 24;
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.Color.LightGray;
            this.panel40.Controls.Add(this.label17);
            this.panel40.Controls.Add(this.panel41);
            this.panel40.Location = new System.Drawing.Point(-9, 555);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(674, 31);
            this.panel40.TabIndex = 62;
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(10, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(633, 24);
            this.label17.TabIndex = 25;
            this.label17.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel41
            // 
            this.panel41.BackColor = System.Drawing.Color.Gray;
            this.panel41.Location = new System.Drawing.Point(0, 30);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(674, 31);
            this.panel41.TabIndex = 24;
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.SystemColors.Window;
            this.panel38.Controls.Add(this.label22);
            this.panel38.Controls.Add(this.panel39);
            this.panel38.Location = new System.Drawing.Point(-9, 713);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(674, 31);
            this.panel38.TabIndex = 67;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(10, 4);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(633, 24);
            this.label22.TabIndex = 29;
            this.label22.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.Gray;
            this.panel39.Controls.Add(this.panel50);
            this.panel39.Controls.Add(this.panel52);
            this.panel39.Location = new System.Drawing.Point(0, 30);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(674, 31);
            this.panel39.TabIndex = 24;
            // 
            // panel50
            // 
            this.panel50.BackColor = System.Drawing.SystemColors.Window;
            this.panel50.Controls.Add(this.panel51);
            this.panel50.Location = new System.Drawing.Point(0, 30);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(674, 31);
            this.panel50.TabIndex = 26;
            // 
            // panel51
            // 
            this.panel51.BackColor = System.Drawing.Color.Gray;
            this.panel51.Location = new System.Drawing.Point(0, 30);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(674, 31);
            this.panel51.TabIndex = 24;
            // 
            // panel52
            // 
            this.panel52.BackColor = System.Drawing.Color.Gray;
            this.panel52.Controls.Add(this.panel61);
            this.panel52.Location = new System.Drawing.Point(0, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(674, 31);
            this.panel52.TabIndex = 25;
            // 
            // panel61
            // 
            this.panel61.BackColor = System.Drawing.Color.Gray;
            this.panel61.Location = new System.Drawing.Point(0, 30);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(674, 31);
            this.panel61.TabIndex = 24;
            // 
            // child_panel_first
            // 
            this.child_panel_first.AutoScroll = true;
            this.child_panel_first.Controls.Add(this.panel127);
            this.child_panel_first.Controls.Add(this.panel38);
            this.child_panel_first.Controls.Add(this.panel40);
            this.child_panel_first.Controls.Add(this.panel12);
            this.child_panel_first.Controls.Add(this.panel59);
            this.child_panel_first.Controls.Add(this.panel6);
            this.child_panel_first.Controls.Add(this.panel42);
            this.child_panel_first.Controls.Add(this.panel34);
            this.child_panel_first.Controls.Add(this.panel62);
            this.child_panel_first.Controls.Add(this.panel33);
            this.child_panel_first.Controls.Add(this.panel2);
            this.child_panel_first.Controls.Add(this.pictureBox3);
            this.child_panel_first.Controls.Add(this.pictureBox2);
            this.child_panel_first.Controls.Add(this.panel36);
            this.child_panel_first.Controls.Add(this.panel25);
            this.child_panel_first.Controls.Add(this.panel23);
            this.child_panel_first.Controls.Add(this.panel3);
            this.child_panel_first.Controls.Add(this.panel53);
            this.child_panel_first.Controls.Add(this.panel31);
            this.child_panel_first.Controls.Add(this.panel17);
            this.child_panel_first.Controls.Add(this.panel48);
            this.child_panel_first.Controls.Add(this.panel16);
            this.child_panel_first.Controls.Add(this.panel14);
            this.child_panel_first.Controls.Add(this.label7);
            this.child_panel_first.Controls.Add(this.panel4);
            this.child_panel_first.Controls.Add(this.child_panel_second);
            this.child_panel_first.Location = new System.Drawing.Point(1, 1);
            this.child_panel_first.Name = "child_panel_first";
            this.child_panel_first.Size = new System.Drawing.Size(686, 797);
            this.child_panel_first.TabIndex = 0;
            // 
            // panel127
            // 
            this.panel127.BackColor = System.Drawing.Color.Black;
            this.panel127.ForeColor = System.Drawing.SystemColors.Window;
            this.panel127.Location = new System.Drawing.Point(1, 138);
            this.panel127.Name = "panel127";
            this.panel127.Size = new System.Drawing.Size(660, 10);
            this.panel127.TabIndex = 72;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Repair_As_New.Properties.Resources._352531_looks_one_1_icon;
            this.pictureBox2.Location = new System.Drawing.Point(579, 21);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(55, 55);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 71;
            this.pictureBox2.TabStop = false;
            // 
            // child_panel_second
            // 
            this.child_panel_second.Controls.Add(this.panel35);
            this.child_panel_second.Controls.Add(this.panel65);
            this.child_panel_second.Controls.Add(this.panel67);
            this.child_panel_second.Controls.Add(this.panel73);
            this.child_panel_second.Controls.Add(this.panel74);
            this.child_panel_second.Controls.Add(this.panel76);
            this.child_panel_second.Controls.Add(this.panel82);
            this.child_panel_second.Controls.Add(this.panel88);
            this.child_panel_second.Controls.Add(this.panel90);
            this.child_panel_second.Controls.Add(this.panel92);
            this.child_panel_second.Controls.Add(this.panel93);
            this.child_panel_second.Controls.Add(this.panel95);
            this.child_panel_second.Controls.Add(this.panel96);
            this.child_panel_second.Controls.Add(this.panel97);
            this.child_panel_second.Controls.Add(this.panel99);
            this.child_panel_second.Controls.Add(this.panel105);
            this.child_panel_second.Controls.Add(this.panel111);
            this.child_panel_second.Controls.Add(this.panel113);
            this.child_panel_second.Controls.Add(this.panel115);
            this.child_panel_second.Controls.Add(this.panel116);
            this.child_panel_second.Controls.Add(this.panel122);
            this.child_panel_second.Controls.Add(this.label45);
            this.child_panel_second.Controls.Add(this.panel123);
            this.child_panel_second.Controls.Add(this.pictureBox1);
            this.child_panel_second.Controls.Add(this.pictureBox4);
            this.child_panel_second.Location = new System.Drawing.Point(-7, 11);
            this.child_panel_second.Name = "child_panel_second";
            this.child_panel_second.Size = new System.Drawing.Size(668, 790);
            this.child_panel_second.TabIndex = 0;
            // 
            // panel35
            // 
            this.panel35.BackColor = System.Drawing.Color.LightGray;
            this.panel35.Controls.Add(this.label25);
            this.panel35.Controls.Add(this.panel64);
            this.panel35.Location = new System.Drawing.Point(-1, 733);
            this.panel35.Name = "panel35";
            this.panel35.Size = new System.Drawing.Size(674, 31);
            this.panel35.TabIndex = 70;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(9, 3);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(633, 24);
            this.label25.TabIndex = 25;
            this.label25.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel64
            // 
            this.panel64.BackColor = System.Drawing.Color.Gray;
            this.panel64.Location = new System.Drawing.Point(0, 30);
            this.panel64.Name = "panel64";
            this.panel64.Size = new System.Drawing.Size(674, 31);
            this.panel64.TabIndex = 24;
            // 
            // panel65
            // 
            this.panel65.BackColor = System.Drawing.Color.LightGray;
            this.panel65.Controls.Add(this.label27);
            this.panel65.Controls.Add(this.panel66);
            this.panel65.Location = new System.Drawing.Point(-1, 612);
            this.panel65.Name = "panel65";
            this.panel65.Size = new System.Drawing.Size(674, 31);
            this.panel65.TabIndex = 66;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(9, 4);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(633, 24);
            this.label27.TabIndex = 25;
            this.label27.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel66
            // 
            this.panel66.BackColor = System.Drawing.Color.Gray;
            this.panel66.Location = new System.Drawing.Point(0, 30);
            this.panel66.Name = "panel66";
            this.panel66.Size = new System.Drawing.Size(674, 31);
            this.panel66.TabIndex = 24;
            // 
            // panel67
            // 
            this.panel67.BackColor = System.Drawing.SystemColors.Window;
            this.panel67.Controls.Add(this.label29);
            this.panel67.Controls.Add(this.panel68);
            this.panel67.Location = new System.Drawing.Point(-2, 703);
            this.panel67.Name = "panel67";
            this.panel67.Size = new System.Drawing.Size(674, 31);
            this.panel67.TabIndex = 69;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.Location = new System.Drawing.Point(10, 4);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(633, 24);
            this.label29.TabIndex = 29;
            this.label29.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel68
            // 
            this.panel68.BackColor = System.Drawing.Color.Gray;
            this.panel68.Controls.Add(this.panel69);
            this.panel68.Controls.Add(this.panel71);
            this.panel68.Location = new System.Drawing.Point(0, 30);
            this.panel68.Name = "panel68";
            this.panel68.Size = new System.Drawing.Size(674, 31);
            this.panel68.TabIndex = 24;
            // 
            // panel69
            // 
            this.panel69.BackColor = System.Drawing.SystemColors.Window;
            this.panel69.Controls.Add(this.panel70);
            this.panel69.Location = new System.Drawing.Point(0, 30);
            this.panel69.Name = "panel69";
            this.panel69.Size = new System.Drawing.Size(674, 31);
            this.panel69.TabIndex = 26;
            // 
            // panel70
            // 
            this.panel70.BackColor = System.Drawing.Color.Gray;
            this.panel70.Location = new System.Drawing.Point(0, 30);
            this.panel70.Name = "panel70";
            this.panel70.Size = new System.Drawing.Size(674, 31);
            this.panel70.TabIndex = 24;
            // 
            // panel71
            // 
            this.panel71.BackColor = System.Drawing.Color.Gray;
            this.panel71.Controls.Add(this.panel72);
            this.panel71.Location = new System.Drawing.Point(0, 0);
            this.panel71.Name = "panel71";
            this.panel71.Size = new System.Drawing.Size(674, 31);
            this.panel71.TabIndex = 25;
            // 
            // panel72
            // 
            this.panel72.BackColor = System.Drawing.Color.Gray;
            this.panel72.Location = new System.Drawing.Point(0, 30);
            this.panel72.Name = "panel72";
            this.panel72.Size = new System.Drawing.Size(674, 31);
            this.panel72.TabIndex = 24;
            // 
            // panel73
            // 
            this.panel73.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel73.Controls.Add(this.label30);
            this.panel73.ForeColor = System.Drawing.SystemColors.Window;
            this.panel73.Location = new System.Drawing.Point(-1, 448);
            this.panel73.Name = "panel73";
            this.panel73.Size = new System.Drawing.Size(674, 37);
            this.panel73.TabIndex = 61;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(205, 7);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(271, 24);
            this.label30.TabIndex = 24;
            this.label30.Text = "2.Ремонт рульової рейки";
            // 
            // panel74
            // 
            this.panel74.BackColor = System.Drawing.Color.LightGray;
            this.panel74.Controls.Add(this.label34);
            this.panel74.Controls.Add(this.panel75);
            this.panel74.Location = new System.Drawing.Point(-1, 388);
            this.panel74.Name = "panel74";
            this.panel74.Size = new System.Drawing.Size(674, 31);
            this.panel74.TabIndex = 59;
            // 
            // label34
            // 
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label34.Location = new System.Drawing.Point(9, 3);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(633, 24);
            this.label34.TabIndex = 25;
            this.label34.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel75
            // 
            this.panel75.BackColor = System.Drawing.Color.Gray;
            this.panel75.Location = new System.Drawing.Point(0, 30);
            this.panel75.Name = "panel75";
            this.panel75.Size = new System.Drawing.Size(674, 31);
            this.panel75.TabIndex = 24;
            // 
            // panel76
            // 
            this.panel76.BackColor = System.Drawing.SystemColors.Window;
            this.panel76.Controls.Add(this.label31);
            this.panel76.Controls.Add(this.panel77);
            this.panel76.Location = new System.Drawing.Point(-1, 201);
            this.panel76.Name = "panel76";
            this.panel76.Size = new System.Drawing.Size(674, 31);
            this.panel76.TabIndex = 53;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(9, 4);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(633, 24);
            this.label31.TabIndex = 29;
            this.label31.Text = "1.2.         Діагностика ходової частини                                         " +
    "                         200";
            // 
            // panel77
            // 
            this.panel77.BackColor = System.Drawing.Color.Gray;
            this.panel77.Controls.Add(this.panel78);
            this.panel77.Controls.Add(this.panel80);
            this.panel77.Location = new System.Drawing.Point(0, 30);
            this.panel77.Name = "panel77";
            this.panel77.Size = new System.Drawing.Size(674, 31);
            this.panel77.TabIndex = 24;
            // 
            // panel78
            // 
            this.panel78.BackColor = System.Drawing.SystemColors.Window;
            this.panel78.Controls.Add(this.panel79);
            this.panel78.Location = new System.Drawing.Point(0, 30);
            this.panel78.Name = "panel78";
            this.panel78.Size = new System.Drawing.Size(674, 31);
            this.panel78.TabIndex = 26;
            // 
            // panel79
            // 
            this.panel79.BackColor = System.Drawing.Color.Gray;
            this.panel79.Location = new System.Drawing.Point(0, 30);
            this.panel79.Name = "panel79";
            this.panel79.Size = new System.Drawing.Size(674, 31);
            this.panel79.TabIndex = 24;
            // 
            // panel80
            // 
            this.panel80.BackColor = System.Drawing.Color.Gray;
            this.panel80.Controls.Add(this.panel81);
            this.panel80.Location = new System.Drawing.Point(0, 0);
            this.panel80.Name = "panel80";
            this.panel80.Size = new System.Drawing.Size(674, 31);
            this.panel80.TabIndex = 25;
            // 
            // panel81
            // 
            this.panel81.BackColor = System.Drawing.Color.Gray;
            this.panel81.Location = new System.Drawing.Point(0, 30);
            this.panel81.Name = "panel81";
            this.panel81.Size = new System.Drawing.Size(674, 31);
            this.panel81.TabIndex = 24;
            // 
            // panel82
            // 
            this.panel82.BackColor = System.Drawing.SystemColors.Window;
            this.panel82.Controls.Add(this.label35);
            this.panel82.Controls.Add(this.panel83);
            this.panel82.Location = new System.Drawing.Point(-2, 515);
            this.panel82.Name = "panel82";
            this.panel82.Size = new System.Drawing.Size(674, 31);
            this.panel82.TabIndex = 63;
            // 
            // label35
            // 
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label35.Location = new System.Drawing.Point(10, 4);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(633, 24);
            this.label35.TabIndex = 29;
            this.label35.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel83
            // 
            this.panel83.BackColor = System.Drawing.Color.Gray;
            this.panel83.Controls.Add(this.panel84);
            this.panel83.Controls.Add(this.panel86);
            this.panel83.Location = new System.Drawing.Point(0, 30);
            this.panel83.Name = "panel83";
            this.panel83.Size = new System.Drawing.Size(674, 31);
            this.panel83.TabIndex = 24;
            // 
            // panel84
            // 
            this.panel84.BackColor = System.Drawing.SystemColors.Window;
            this.panel84.Controls.Add(this.panel85);
            this.panel84.Location = new System.Drawing.Point(0, 30);
            this.panel84.Name = "panel84";
            this.panel84.Size = new System.Drawing.Size(674, 31);
            this.panel84.TabIndex = 26;
            // 
            // panel85
            // 
            this.panel85.BackColor = System.Drawing.Color.Gray;
            this.panel85.Location = new System.Drawing.Point(0, 30);
            this.panel85.Name = "panel85";
            this.panel85.Size = new System.Drawing.Size(674, 31);
            this.panel85.TabIndex = 24;
            // 
            // panel86
            // 
            this.panel86.BackColor = System.Drawing.Color.Gray;
            this.panel86.Controls.Add(this.panel87);
            this.panel86.Location = new System.Drawing.Point(0, 0);
            this.panel86.Name = "panel86";
            this.panel86.Size = new System.Drawing.Size(674, 31);
            this.panel86.TabIndex = 25;
            // 
            // panel87
            // 
            this.panel87.BackColor = System.Drawing.Color.Gray;
            this.panel87.Location = new System.Drawing.Point(0, 30);
            this.panel87.Name = "panel87";
            this.panel87.Size = new System.Drawing.Size(674, 31);
            this.panel87.TabIndex = 24;
            // 
            // panel88
            // 
            this.panel88.BackColor = System.Drawing.Color.LightGray;
            this.panel88.Controls.Add(this.label37);
            this.panel88.Controls.Add(this.panel89);
            this.panel88.Location = new System.Drawing.Point(-1, 545);
            this.panel88.Name = "panel88";
            this.panel88.Size = new System.Drawing.Size(674, 31);
            this.panel88.TabIndex = 64;
            // 
            // label37
            // 
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label37.Location = new System.Drawing.Point(9, 3);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(633, 24);
            this.label37.TabIndex = 25;
            this.label37.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel89
            // 
            this.panel89.BackColor = System.Drawing.Color.Gray;
            this.panel89.Location = new System.Drawing.Point(0, 30);
            this.panel89.Name = "panel89";
            this.panel89.Size = new System.Drawing.Size(674, 31);
            this.panel89.TabIndex = 24;
            // 
            // panel90
            // 
            this.panel90.BackColor = System.Drawing.SystemColors.Window;
            this.panel90.Controls.Add(this.label36);
            this.panel90.Controls.Add(this.panel91);
            this.panel90.Location = new System.Drawing.Point(-1, 261);
            this.panel90.Name = "panel90";
            this.panel90.Size = new System.Drawing.Size(674, 31);
            this.panel90.TabIndex = 55;
            // 
            // label36
            // 
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label36.Location = new System.Drawing.Point(9, 4);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(633, 24);
            this.label36.TabIndex = 30;
            this.label36.Text = "1.1.         Комп’ютерна діагностика                                             " +
    "                           200";
            // 
            // panel91
            // 
            this.panel91.BackColor = System.Drawing.Color.Gray;
            this.panel91.Location = new System.Drawing.Point(0, 30);
            this.panel91.Name = "panel91";
            this.panel91.Size = new System.Drawing.Size(674, 31);
            this.panel91.TabIndex = 24;
            // 
            // panel92
            // 
            this.panel92.BackColor = System.Drawing.Color.Black;
            this.panel92.ForeColor = System.Drawing.SystemColors.Window;
            this.panel92.Location = new System.Drawing.Point(-1, 127);
            this.panel92.Name = "panel92";
            this.panel92.Size = new System.Drawing.Size(693, 10);
            this.panel92.TabIndex = 51;
            // 
            // panel93
            // 
            this.panel93.BackColor = System.Drawing.Color.LightGray;
            this.panel93.Controls.Add(this.label41);
            this.panel93.Controls.Add(this.panel94);
            this.panel93.Location = new System.Drawing.Point(-1, 673);
            this.panel93.Name = "panel93";
            this.panel93.Size = new System.Drawing.Size(674, 31);
            this.panel93.TabIndex = 68;
            // 
            // label41
            // 
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label41.Location = new System.Drawing.Point(10, 4);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(633, 24);
            this.label41.TabIndex = 25;
            this.label41.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel94
            // 
            this.panel94.BackColor = System.Drawing.Color.Gray;
            this.panel94.Location = new System.Drawing.Point(0, 30);
            this.panel94.Name = "panel94";
            this.panel94.Size = new System.Drawing.Size(674, 31);
            this.panel94.TabIndex = 24;
            // 
            // panel95
            // 
            this.panel95.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel95.Controls.Add(this.label39);
            this.panel95.ForeColor = System.Drawing.SystemColors.Window;
            this.panel95.Location = new System.Drawing.Point(-1, 575);
            this.panel95.Name = "panel95";
            this.panel95.Size = new System.Drawing.Size(674, 37);
            this.panel95.TabIndex = 65;
            // 
            // label39
            // 
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label39.Location = new System.Drawing.Point(205, 7);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(271, 24);
            this.label39.TabIndex = 24;
            this.label39.Text = "2.Ремонт рульової рейки";
            // 
            // panel96
            // 
            this.panel96.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel96.Controls.Add(this.label42);
            this.panel96.ForeColor = System.Drawing.SystemColors.Window;
            this.panel96.Location = new System.Drawing.Point(-1, 291);
            this.panel96.Name = "panel96";
            this.panel96.Size = new System.Drawing.Size(674, 37);
            this.panel96.TabIndex = 56;
            // 
            // label42
            // 
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label42.Location = new System.Drawing.Point(205, 7);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(271, 24);
            this.label42.TabIndex = 24;
            this.label42.Text = "2.Ремонт рульової рейки";
            // 
            // panel97
            // 
            this.panel97.BackColor = System.Drawing.Color.LightGray;
            this.panel97.Controls.Add(this.label28);
            this.panel97.Controls.Add(this.panel98);
            this.panel97.Location = new System.Drawing.Point(-1, 231);
            this.panel97.Name = "panel97";
            this.panel97.Size = new System.Drawing.Size(674, 31);
            this.panel97.TabIndex = 54;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(9, 5);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(633, 24);
            this.label28.TabIndex = 30;
            this.label28.Text = "1.3.         Діагностика гальмівної системи                                      " +
    "                      200";
            // 
            // panel98
            // 
            this.panel98.BackColor = System.Drawing.Color.Gray;
            this.panel98.Location = new System.Drawing.Point(0, 30);
            this.panel98.Name = "panel98";
            this.panel98.Size = new System.Drawing.Size(674, 31);
            this.panel98.TabIndex = 24;
            // 
            // panel99
            // 
            this.panel99.BackColor = System.Drawing.SystemColors.Window;
            this.panel99.Controls.Add(this.label21);
            this.panel99.Controls.Add(this.panel100);
            this.panel99.Location = new System.Drawing.Point(-1, 642);
            this.panel99.Name = "panel99";
            this.panel99.Size = new System.Drawing.Size(674, 31);
            this.panel99.TabIndex = 67;
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(9, 4);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(633, 24);
            this.label21.TabIndex = 29;
            this.label21.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel100
            // 
            this.panel100.BackColor = System.Drawing.Color.Gray;
            this.panel100.Controls.Add(this.panel101);
            this.panel100.Controls.Add(this.panel103);
            this.panel100.Location = new System.Drawing.Point(0, 30);
            this.panel100.Name = "panel100";
            this.panel100.Size = new System.Drawing.Size(674, 31);
            this.panel100.TabIndex = 24;
            // 
            // panel101
            // 
            this.panel101.BackColor = System.Drawing.SystemColors.Window;
            this.panel101.Controls.Add(this.panel102);
            this.panel101.Location = new System.Drawing.Point(0, 30);
            this.panel101.Name = "panel101";
            this.panel101.Size = new System.Drawing.Size(674, 31);
            this.panel101.TabIndex = 26;
            // 
            // panel102
            // 
            this.panel102.BackColor = System.Drawing.Color.Gray;
            this.panel102.Location = new System.Drawing.Point(0, 30);
            this.panel102.Name = "panel102";
            this.panel102.Size = new System.Drawing.Size(674, 31);
            this.panel102.TabIndex = 24;
            // 
            // panel103
            // 
            this.panel103.BackColor = System.Drawing.Color.Gray;
            this.panel103.Controls.Add(this.panel104);
            this.panel103.Location = new System.Drawing.Point(0, 0);
            this.panel103.Name = "panel103";
            this.panel103.Size = new System.Drawing.Size(674, 31);
            this.panel103.TabIndex = 25;
            // 
            // panel104
            // 
            this.panel104.BackColor = System.Drawing.Color.Gray;
            this.panel104.Location = new System.Drawing.Point(0, 30);
            this.panel104.Name = "panel104";
            this.panel104.Size = new System.Drawing.Size(674, 31);
            this.panel104.TabIndex = 24;
            // 
            // panel105
            // 
            this.panel105.BackColor = System.Drawing.SystemColors.Window;
            this.panel105.Controls.Add(this.label33);
            this.panel105.Controls.Add(this.panel106);
            this.panel105.Location = new System.Drawing.Point(-1, 418);
            this.panel105.Name = "panel105";
            this.panel105.Size = new System.Drawing.Size(674, 31);
            this.panel105.TabIndex = 60;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.Location = new System.Drawing.Point(9, 3);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(633, 24);
            this.label33.TabIndex = 29;
            this.label33.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel106
            // 
            this.panel106.BackColor = System.Drawing.Color.Gray;
            this.panel106.Controls.Add(this.panel107);
            this.panel106.Controls.Add(this.panel109);
            this.panel106.Location = new System.Drawing.Point(0, 30);
            this.panel106.Name = "panel106";
            this.panel106.Size = new System.Drawing.Size(674, 31);
            this.panel106.TabIndex = 24;
            // 
            // panel107
            // 
            this.panel107.BackColor = System.Drawing.SystemColors.Window;
            this.panel107.Controls.Add(this.panel108);
            this.panel107.Location = new System.Drawing.Point(0, 30);
            this.panel107.Name = "panel107";
            this.panel107.Size = new System.Drawing.Size(674, 31);
            this.panel107.TabIndex = 26;
            // 
            // panel108
            // 
            this.panel108.BackColor = System.Drawing.Color.Gray;
            this.panel108.Location = new System.Drawing.Point(0, 30);
            this.panel108.Name = "panel108";
            this.panel108.Size = new System.Drawing.Size(674, 31);
            this.panel108.TabIndex = 24;
            // 
            // panel109
            // 
            this.panel109.BackColor = System.Drawing.Color.Gray;
            this.panel109.Controls.Add(this.panel110);
            this.panel109.Location = new System.Drawing.Point(0, 0);
            this.panel109.Name = "panel109";
            this.panel109.Size = new System.Drawing.Size(674, 31);
            this.panel109.TabIndex = 25;
            // 
            // panel110
            // 
            this.panel110.BackColor = System.Drawing.Color.Gray;
            this.panel110.Location = new System.Drawing.Point(0, 30);
            this.panel110.Name = "panel110";
            this.panel110.Size = new System.Drawing.Size(674, 31);
            this.panel110.TabIndex = 24;
            // 
            // panel111
            // 
            this.panel111.BackColor = System.Drawing.Color.LightGray;
            this.panel111.Controls.Add(this.label43);
            this.panel111.Controls.Add(this.panel112);
            this.panel111.Location = new System.Drawing.Point(-1, 485);
            this.panel111.Name = "panel111";
            this.panel111.Size = new System.Drawing.Size(674, 31);
            this.panel111.TabIndex = 62;
            // 
            // label43
            // 
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label43.Location = new System.Drawing.Point(9, 4);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(633, 24);
            this.label43.TabIndex = 25;
            this.label43.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel112
            // 
            this.panel112.BackColor = System.Drawing.Color.Gray;
            this.panel112.Location = new System.Drawing.Point(0, 30);
            this.panel112.Name = "panel112";
            this.panel112.Size = new System.Drawing.Size(674, 31);
            this.panel112.TabIndex = 24;
            // 
            // panel113
            // 
            this.panel113.BackColor = System.Drawing.Color.LightGray;
            this.panel113.Controls.Add(this.label38);
            this.panel113.Controls.Add(this.panel114);
            this.panel113.Location = new System.Drawing.Point(-1, 328);
            this.panel113.Name = "panel113";
            this.panel113.Size = new System.Drawing.Size(674, 31);
            this.panel113.TabIndex = 57;
            // 
            // label38
            // 
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label38.Location = new System.Drawing.Point(9, 4);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(633, 24);
            this.label38.TabIndex = 25;
            this.label38.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel114
            // 
            this.panel114.BackColor = System.Drawing.Color.Gray;
            this.panel114.Location = new System.Drawing.Point(0, 30);
            this.panel114.Name = "panel114";
            this.panel114.Size = new System.Drawing.Size(674, 31);
            this.panel114.TabIndex = 24;
            // 
            // panel115
            // 
            this.panel115.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel115.Controls.Add(this.label44);
            this.panel115.Controls.Add(this.label40);
            this.panel115.Controls.Add(this.panel125);
            this.panel115.ForeColor = System.Drawing.SystemColors.Window;
            this.panel115.Location = new System.Drawing.Point(-1, 91);
            this.panel115.Name = "panel115";
            this.panel115.Size = new System.Drawing.Size(684, 41);
            this.panel115.TabIndex = 49;
            // 
            // label44
            // 
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label44.Location = new System.Drawing.Point(495, 7);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 24);
            this.label44.TabIndex = 23;
            this.label44.Text = "Ціна, грн*";
            // 
            // label40
            // 
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label40.Location = new System.Drawing.Point(46, 7);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(174, 24);
            this.label40.TabIndex = 21;
            this.label40.Text = "Найменування";
            // 
            // panel125
            // 
            this.panel125.Location = new System.Drawing.Point(218, 34);
            this.panel125.Name = "panel125";
            this.panel125.Size = new System.Drawing.Size(676, 764);
            this.panel125.TabIndex = 72;
            // 
            // panel116
            // 
            this.panel116.BackColor = System.Drawing.SystemColors.Window;
            this.panel116.Controls.Add(this.label32);
            this.panel116.Controls.Add(this.panel117);
            this.panel116.Location = new System.Drawing.Point(-1, 358);
            this.panel116.Name = "panel116";
            this.panel116.Size = new System.Drawing.Size(674, 31);
            this.panel116.TabIndex = 58;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(9, 4);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(633, 24);
            this.label32.TabIndex = 29;
            this.label32.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel117
            // 
            this.panel117.BackColor = System.Drawing.Color.Gray;
            this.panel117.Controls.Add(this.panel118);
            this.panel117.Controls.Add(this.panel120);
            this.panel117.Location = new System.Drawing.Point(0, 30);
            this.panel117.Name = "panel117";
            this.panel117.Size = new System.Drawing.Size(674, 31);
            this.panel117.TabIndex = 24;
            // 
            // panel118
            // 
            this.panel118.BackColor = System.Drawing.SystemColors.Window;
            this.panel118.Controls.Add(this.panel119);
            this.panel118.Location = new System.Drawing.Point(0, 30);
            this.panel118.Name = "panel118";
            this.panel118.Size = new System.Drawing.Size(674, 31);
            this.panel118.TabIndex = 26;
            // 
            // panel119
            // 
            this.panel119.BackColor = System.Drawing.Color.Gray;
            this.panel119.Location = new System.Drawing.Point(0, 30);
            this.panel119.Name = "panel119";
            this.panel119.Size = new System.Drawing.Size(674, 31);
            this.panel119.TabIndex = 24;
            // 
            // panel120
            // 
            this.panel120.BackColor = System.Drawing.Color.Gray;
            this.panel120.Controls.Add(this.panel121);
            this.panel120.Location = new System.Drawing.Point(0, 0);
            this.panel120.Name = "panel120";
            this.panel120.Size = new System.Drawing.Size(674, 31);
            this.panel120.TabIndex = 25;
            // 
            // panel121
            // 
            this.panel121.BackColor = System.Drawing.Color.Gray;
            this.panel121.Location = new System.Drawing.Point(0, 30);
            this.panel121.Name = "panel121";
            this.panel121.Size = new System.Drawing.Size(674, 31);
            this.panel121.TabIndex = 24;
            // 
            // panel122
            // 
            this.panel122.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel122.Controls.Add(this.label26);
            this.panel122.ForeColor = System.Drawing.SystemColors.Window;
            this.panel122.Location = new System.Drawing.Point(-1, 134);
            this.panel122.Name = "panel122";
            this.panel122.Size = new System.Drawing.Size(684, 37);
            this.panel122.TabIndex = 50;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(248, 5);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(174, 24);
            this.label26.TabIndex = 24;
            this.label26.Text = "1.Діагностика автомобіля";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label45.Location = new System.Drawing.Point(274, 8);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(66, 29);
            this.label45.TabIndex = 47;
            this.label45.Text = "Ціни";
            // 
            // panel123
            // 
            this.panel123.BackColor = System.Drawing.Color.LightGray;
            this.panel123.Controls.Add(this.label46);
            this.panel123.Controls.Add(this.panel124);
            this.panel123.Location = new System.Drawing.Point(-1, 171);
            this.panel123.Name = "panel123";
            this.panel123.Size = new System.Drawing.Size(674, 31);
            this.panel123.TabIndex = 52;
            // 
            // label46
            // 
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label46.Location = new System.Drawing.Point(9, 4);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(633, 24);
            this.label46.TabIndex = 25;
            this.label46.Text = "1.1.         Комп’ютерна діагностика                                             " +
    "                           200";
            // 
            // panel124
            // 
            this.panel124.BackColor = System.Drawing.Color.Gray;
            this.panel124.Location = new System.Drawing.Point(0, 30);
            this.panel124.Name = "panel124";
            this.panel124.Size = new System.Drawing.Size(674, 31);
            this.panel124.TabIndex = 24;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Repair_As_New.Properties.Resources._352532_looks_2_two_icon;
            this.pictureBox1.Location = new System.Drawing.Point(584, 10);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(55, 55);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 72;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Repair_As_New.Properties.Resources._172529_price_usd_tag_icon__2_;
            this.pictureBox4.Location = new System.Drawing.Point(187, 10);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(55, 55);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 48;
            this.pictureBox4.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.roundButton2);
            this.panel1.Controls.Add(this.roundButton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 804);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(687, 60);
            this.panel1.TabIndex = 1;
            // 
            // roundButton2
            // 
            this.roundButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.roundButton2.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.roundButton2.BorderColor = System.Drawing.Color.PaleVioletRed;
            this.roundButton2.BorderRadius = 20;
            this.roundButton2.BorderSize = 0;
            this.roundButton2.FlatAppearance.BorderSize = 0;
            this.roundButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundButton2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.roundButton2.ForeColor = System.Drawing.Color.White;
            this.roundButton2.Location = new System.Drawing.Point(145, 8);
            this.roundButton2.Name = "roundButton2";
            this.roundButton2.Size = new System.Drawing.Size(150, 40);
            this.roundButton2.TabIndex = 74;
            this.roundButton2.Text = "Назад";
            this.roundButton2.TextColor = System.Drawing.Color.White;
            this.roundButton2.UseVisualStyleBackColor = false;
            this.roundButton2.Click += new System.EventHandler(this.roundButton2_Click);
            // 
            // roundButton1
            // 
            this.roundButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.roundButton1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.roundButton1.BorderColor = System.Drawing.Color.PaleVioletRed;
            this.roundButton1.BorderRadius = 20;
            this.roundButton1.BorderSize = 0;
            this.roundButton1.FlatAppearance.BorderSize = 0;
            this.roundButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.roundButton1.ForeColor = System.Drawing.Color.White;
            this.roundButton1.Location = new System.Drawing.Point(354, 8);
            this.roundButton1.Name = "roundButton1";
            this.roundButton1.Size = new System.Drawing.Size(150, 40);
            this.roundButton1.TabIndex = 73;
            this.roundButton1.Text = "Далі";
            this.roundButton1.TextColor = System.Drawing.Color.White;
            this.roundButton1.UseVisualStyleBackColor = false;
            this.roundButton1.Click += new System.EventHandler(this.roundButton1_Click_1);
            // 
            // panel126
            // 
            this.panel126.BackColor = System.Drawing.Color.Black;
            this.panel126.ForeColor = System.Drawing.SystemColors.Window;
            this.panel126.Location = new System.Drawing.Point(3, 139);
            this.panel126.Name = "panel126";
            this.panel126.Size = new System.Drawing.Size(674, 8);
            this.panel126.TabIndex = 52;
            // 
            // Prices_Form
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(657, 881);
            this.Controls.Add(this.child_panel_first);
            this.Controls.Add(this.panel126);
            this.Controls.Add(this.panel1);
            this.Name = "Prices_Form";
            this.Text = "Prices_Form";
            this.Load += new System.EventHandler(this.Prices_Form_Load);
            this.panel4.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel48.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.panel18.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            this.panel54.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panel57.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel23.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel44.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.panel50.ResumeLayout(false);
            this.panel52.ResumeLayout(false);
            this.child_panel_first.ResumeLayout(false);
            this.child_panel_first.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.child_panel_second.ResumeLayout(false);
            this.child_panel_second.PerformLayout();
            this.panel35.ResumeLayout(false);
            this.panel65.ResumeLayout(false);
            this.panel67.ResumeLayout(false);
            this.panel68.ResumeLayout(false);
            this.panel69.ResumeLayout(false);
            this.panel71.ResumeLayout(false);
            this.panel73.ResumeLayout(false);
            this.panel74.ResumeLayout(false);
            this.panel76.ResumeLayout(false);
            this.panel77.ResumeLayout(false);
            this.panel78.ResumeLayout(false);
            this.panel80.ResumeLayout(false);
            this.panel82.ResumeLayout(false);
            this.panel83.ResumeLayout(false);
            this.panel84.ResumeLayout(false);
            this.panel86.ResumeLayout(false);
            this.panel88.ResumeLayout(false);
            this.panel90.ResumeLayout(false);
            this.panel93.ResumeLayout(false);
            this.panel95.ResumeLayout(false);
            this.panel96.ResumeLayout(false);
            this.panel97.ResumeLayout(false);
            this.panel99.ResumeLayout(false);
            this.panel100.ResumeLayout(false);
            this.panel101.ResumeLayout(false);
            this.panel103.ResumeLayout(false);
            this.panel105.ResumeLayout(false);
            this.panel106.ResumeLayout(false);
            this.panel107.ResumeLayout(false);
            this.panel109.ResumeLayout(false);
            this.panel111.ResumeLayout(false);
            this.panel113.ResumeLayout(false);
            this.panel115.ResumeLayout(false);
            this.panel116.ResumeLayout(false);
            this.panel117.ResumeLayout(false);
            this.panel118.ResumeLayout(false);
            this.panel120.ResumeLayout(false);
            this.panel122.ResumeLayout(false);
            this.panel123.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Panel child_panel_first;
        private System.Windows.Forms.Panel panel1;
        private RoundButton roundButton2;
        private RoundButton roundButton1;
        private System.Windows.Forms.Panel child_panel_second;
        private System.Windows.Forms.Panel panel35;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Panel panel64;
        private System.Windows.Forms.Panel panel65;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Panel panel66;
        private System.Windows.Forms.Panel panel67;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Panel panel68;
        private System.Windows.Forms.Panel panel69;
        private System.Windows.Forms.Panel panel70;
        private System.Windows.Forms.Panel panel71;
        private System.Windows.Forms.Panel panel72;
        private System.Windows.Forms.Panel panel73;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Panel panel74;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel75;
        private System.Windows.Forms.Panel panel76;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel panel77;
        private System.Windows.Forms.Panel panel78;
        private System.Windows.Forms.Panel panel79;
        private System.Windows.Forms.Panel panel80;
        private System.Windows.Forms.Panel panel81;
        private System.Windows.Forms.Panel panel82;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Panel panel83;
        private System.Windows.Forms.Panel panel84;
        private System.Windows.Forms.Panel panel85;
        private System.Windows.Forms.Panel panel86;
        private System.Windows.Forms.Panel panel87;
        private System.Windows.Forms.Panel panel88;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Panel panel89;
        private System.Windows.Forms.Panel panel90;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Panel panel91;
        private System.Windows.Forms.Panel panel92;
        private System.Windows.Forms.Panel panel93;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Panel panel94;
        private System.Windows.Forms.Panel panel95;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Panel panel96;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Panel panel97;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel98;
        private System.Windows.Forms.Panel panel99;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Panel panel100;
        private System.Windows.Forms.Panel panel101;
        private System.Windows.Forms.Panel panel102;
        private System.Windows.Forms.Panel panel103;
        private System.Windows.Forms.Panel panel104;
        private System.Windows.Forms.Panel panel105;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Panel panel106;
        private System.Windows.Forms.Panel panel107;
        private System.Windows.Forms.Panel panel108;
        private System.Windows.Forms.Panel panel109;
        private System.Windows.Forms.Panel panel110;
        private System.Windows.Forms.Panel panel111;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Panel panel112;
        private System.Windows.Forms.Panel panel113;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Panel panel114;
        private System.Windows.Forms.Panel panel115;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Panel panel125;
        private System.Windows.Forms.Panel panel116;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Panel panel117;
        private System.Windows.Forms.Panel panel118;
        private System.Windows.Forms.Panel panel119;
        private System.Windows.Forms.Panel panel120;
        private System.Windows.Forms.Panel panel121;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel122;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel123;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Panel panel124;
        private System.Windows.Forms.Panel panel126;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel127;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}