﻿
namespace Repair_As_New
{
    partial class Prices_Form_II
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel24 = new System.Windows.Forms.Panel();
            this.panel23 = new System.Windows.Forms.Panel();
            this.panel63 = new System.Windows.Forms.Panel();
            this.panel62 = new System.Windows.Forms.Panel();
            this.panel33 = new System.Windows.Forms.Panel();
            this.panel34 = new System.Windows.Forms.Panel();
            this.panel54 = new System.Windows.Forms.Panel();
            this.panel57 = new System.Windows.Forms.Panel();
            this.panel58 = new System.Windows.Forms.Panel();
            this.panel55 = new System.Windows.Forms.Panel();
            this.panel56 = new System.Windows.Forms.Panel();
            this.panel53 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.panel37 = new System.Windows.Forms.Panel();
            this.panel36 = new System.Windows.Forms.Panel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel60 = new System.Windows.Forms.Panel();
            this.panel59 = new System.Windows.Forms.Panel();
            this.panel43 = new System.Windows.Forms.Panel();
            this.panel46 = new System.Windows.Forms.Panel();
            this.panel47 = new System.Windows.Forms.Panel();
            this.panel44 = new System.Windows.Forms.Panel();
            this.panel45 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.panel42 = new System.Windows.Forms.Panel();
            this.panel41 = new System.Windows.Forms.Panel();
            this.panel40 = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.panel39 = new System.Windows.Forms.Panel();
            this.panel52 = new System.Windows.Forms.Panel();
            this.panel61 = new System.Windows.Forms.Panel();
            this.panel50 = new System.Windows.Forms.Panel();
            this.panel51 = new System.Windows.Forms.Panel();
            this.label22 = new System.Windows.Forms.Label();
            this.panel38 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.child_panel_second = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel16 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel32 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.panel31 = new System.Windows.Forms.Panel();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel26 = new System.Windows.Forms.Panel();
            this.panel29 = new System.Windows.Forms.Panel();
            this.panel30 = new System.Windows.Forms.Panel();
            this.panel27 = new System.Windows.Forms.Panel();
            this.panel28 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.panel25 = new System.Windows.Forms.Panel();
            this.panel49 = new System.Windows.Forms.Panel();
            this.panel48 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel18 = new System.Windows.Forms.Panel();
            this.panel21 = new System.Windows.Forms.Panel();
            this.panel22 = new System.Windows.Forms.Panel();
            this.panel19 = new System.Windows.Forms.Panel();
            this.panel20 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.panel23.SuspendLayout();
            this.panel62.SuspendLayout();
            this.panel33.SuspendLayout();
            this.panel34.SuspendLayout();
            this.panel54.SuspendLayout();
            this.panel57.SuspendLayout();
            this.panel55.SuspendLayout();
            this.panel53.SuspendLayout();
            this.panel36.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel59.SuspendLayout();
            this.panel43.SuspendLayout();
            this.panel46.SuspendLayout();
            this.panel44.SuspendLayout();
            this.panel42.SuspendLayout();
            this.panel40.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel39.SuspendLayout();
            this.panel52.SuspendLayout();
            this.panel50.SuspendLayout();
            this.panel38.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel31.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel26.SuspendLayout();
            this.panel29.SuspendLayout();
            this.panel27.SuspendLayout();
            this.panel25.SuspendLayout();
            this.panel48.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel18.SuspendLayout();
            this.panel21.SuspendLayout();
            this.panel19.SuspendLayout();
            this.panel17.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel24
            // 
            this.panel24.BackColor = System.Drawing.Color.Gray;
            this.panel24.Location = new System.Drawing.Point(0, 30);
            this.panel24.Name = "panel24";
            this.panel24.Size = new System.Drawing.Size(674, 31);
            this.panel24.TabIndex = 24;
            // 
            // panel23
            // 
            this.panel23.BackColor = System.Drawing.Color.Gray;
            this.panel23.Controls.Add(this.label10);
            this.panel23.Controls.Add(this.panel24);
            this.panel23.Location = new System.Drawing.Point(2, 328);
            this.panel23.Name = "panel23";
            this.panel23.Size = new System.Drawing.Size(674, 31);
            this.panel23.TabIndex = 28;
            // 
            // panel63
            // 
            this.panel63.BackColor = System.Drawing.Color.Gray;
            this.panel63.Location = new System.Drawing.Point(0, 30);
            this.panel63.Name = "panel63";
            this.panel63.Size = new System.Drawing.Size(674, 31);
            this.panel63.TabIndex = 24;
            // 
            // panel62
            // 
            this.panel62.BackColor = System.Drawing.Color.Gray;
            this.panel62.Controls.Add(this.label23);
            this.panel62.Controls.Add(this.panel63);
            this.panel62.Location = new System.Drawing.Point(2, 733);
            this.panel62.Name = "panel62";
            this.panel62.Size = new System.Drawing.Size(674, 31);
            this.panel62.TabIndex = 42;
            // 
            // panel33
            // 
            this.panel33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel33.Controls.Add(this.label14);
            this.panel33.ForeColor = System.Drawing.SystemColors.Window;
            this.panel33.Location = new System.Drawing.Point(2, 448);
            this.panel33.Name = "panel33";
            this.panel33.Size = new System.Drawing.Size(674, 37);
            this.panel33.TabIndex = 32;
            // 
            // panel34
            // 
            this.panel34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel34.Controls.Add(this.label18);
            this.panel34.ForeColor = System.Drawing.SystemColors.Window;
            this.panel34.Location = new System.Drawing.Point(2, 575);
            this.panel34.Name = "panel34";
            this.panel34.Size = new System.Drawing.Size(674, 37);
            this.panel34.TabIndex = 36;
            // 
            // panel54
            // 
            this.panel54.BackColor = System.Drawing.Color.Gray;
            this.panel54.Controls.Add(this.panel55);
            this.panel54.Controls.Add(this.panel57);
            this.panel54.Location = new System.Drawing.Point(0, 30);
            this.panel54.Name = "panel54";
            this.panel54.Size = new System.Drawing.Size(674, 31);
            this.panel54.TabIndex = 24;
            // 
            // panel57
            // 
            this.panel57.BackColor = System.Drawing.Color.Gray;
            this.panel57.Controls.Add(this.panel58);
            this.panel57.Location = new System.Drawing.Point(0, 0);
            this.panel57.Name = "panel57";
            this.panel57.Size = new System.Drawing.Size(674, 31);
            this.panel57.TabIndex = 25;
            // 
            // panel58
            // 
            this.panel58.BackColor = System.Drawing.Color.Gray;
            this.panel58.Location = new System.Drawing.Point(0, 30);
            this.panel58.Name = "panel58";
            this.panel58.Size = new System.Drawing.Size(674, 31);
            this.panel58.TabIndex = 24;
            // 
            // panel55
            // 
            this.panel55.BackColor = System.Drawing.SystemColors.Window;
            this.panel55.Controls.Add(this.panel56);
            this.panel55.Location = new System.Drawing.Point(0, 30);
            this.panel55.Name = "panel55";
            this.panel55.Size = new System.Drawing.Size(674, 31);
            this.panel55.TabIndex = 26;
            // 
            // panel56
            // 
            this.panel56.BackColor = System.Drawing.Color.Gray;
            this.panel56.Location = new System.Drawing.Point(0, 30);
            this.panel56.Name = "panel56";
            this.panel56.Size = new System.Drawing.Size(674, 31);
            this.panel56.TabIndex = 24;
            // 
            // panel53
            // 
            this.panel53.BackColor = System.Drawing.SystemColors.Window;
            this.panel53.Controls.Add(this.label19);
            this.panel53.Controls.Add(this.panel54);
            this.panel53.Location = new System.Drawing.Point(2, 642);
            this.panel53.Name = "panel53";
            this.panel53.Size = new System.Drawing.Size(674, 31);
            this.panel53.TabIndex = 38;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(10, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(633, 24);
            this.label10.TabIndex = 25;
            this.label10.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(10, 3);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(633, 24);
            this.label23.TabIndex = 25;
            this.label23.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel37
            // 
            this.panel37.BackColor = System.Drawing.Color.Gray;
            this.panel37.Location = new System.Drawing.Point(0, 30);
            this.panel37.Name = "panel37";
            this.panel37.Size = new System.Drawing.Size(674, 31);
            this.panel37.TabIndex = 24;
            // 
            // panel36
            // 
            this.panel36.BackColor = System.Drawing.Color.Gray;
            this.panel36.Controls.Add(this.label24);
            this.panel36.Controls.Add(this.panel37);
            this.panel36.Location = new System.Drawing.Point(2, 673);
            this.panel36.Name = "panel36";
            this.panel36.Size = new System.Drawing.Size(674, 31);
            this.panel36.TabIndex = 40;
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.Gray;
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Controls.Add(this.panel10);
            this.panel7.Location = new System.Drawing.Point(0, 30);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(674, 31);
            this.panel7.TabIndex = 24;
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.Gray;
            this.panel10.Controls.Add(this.panel11);
            this.panel10.Location = new System.Drawing.Point(0, 0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(674, 31);
            this.panel10.TabIndex = 25;
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.Gray;
            this.panel11.Location = new System.Drawing.Point(0, 30);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(674, 31);
            this.panel11.TabIndex = 24;
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.SystemColors.Window;
            this.panel8.Controls.Add(this.panel9);
            this.panel8.Location = new System.Drawing.Point(0, 30);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(674, 31);
            this.panel8.TabIndex = 26;
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.Gray;
            this.panel9.Location = new System.Drawing.Point(0, 30);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(674, 31);
            this.panel9.TabIndex = 24;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.Window;
            this.panel6.Controls.Add(this.label5);
            this.panel6.Controls.Add(this.panel7);
            this.panel6.Location = new System.Drawing.Point(2, 201);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(674, 31);
            this.panel6.TabIndex = 24;
            // 
            // panel60
            // 
            this.panel60.BackColor = System.Drawing.Color.Gray;
            this.panel60.Location = new System.Drawing.Point(0, 30);
            this.panel60.Name = "panel60";
            this.panel60.Size = new System.Drawing.Size(674, 31);
            this.panel60.TabIndex = 24;
            // 
            // panel59
            // 
            this.panel59.BackColor = System.Drawing.Color.Gray;
            this.panel59.Controls.Add(this.label20);
            this.panel59.Controls.Add(this.panel60);
            this.panel59.Location = new System.Drawing.Point(2, 612);
            this.panel59.Name = "panel59";
            this.panel59.Size = new System.Drawing.Size(674, 31);
            this.panel59.TabIndex = 37;
            // 
            // panel43
            // 
            this.panel43.BackColor = System.Drawing.Color.Gray;
            this.panel43.Controls.Add(this.panel44);
            this.panel43.Controls.Add(this.panel46);
            this.panel43.Location = new System.Drawing.Point(0, 30);
            this.panel43.Name = "panel43";
            this.panel43.Size = new System.Drawing.Size(674, 31);
            this.panel43.TabIndex = 24;
            // 
            // panel46
            // 
            this.panel46.BackColor = System.Drawing.Color.Gray;
            this.panel46.Controls.Add(this.panel47);
            this.panel46.Location = new System.Drawing.Point(0, 0);
            this.panel46.Name = "panel46";
            this.panel46.Size = new System.Drawing.Size(674, 31);
            this.panel46.TabIndex = 25;
            // 
            // panel47
            // 
            this.panel47.BackColor = System.Drawing.Color.Gray;
            this.panel47.Location = new System.Drawing.Point(0, 30);
            this.panel47.Name = "panel47";
            this.panel47.Size = new System.Drawing.Size(674, 31);
            this.panel47.TabIndex = 24;
            // 
            // panel44
            // 
            this.panel44.BackColor = System.Drawing.SystemColors.Window;
            this.panel44.Controls.Add(this.panel45);
            this.panel44.Location = new System.Drawing.Point(0, 30);
            this.panel44.Name = "panel44";
            this.panel44.Size = new System.Drawing.Size(674, 31);
            this.panel44.TabIndex = 26;
            // 
            // panel45
            // 
            this.panel45.BackColor = System.Drawing.Color.Gray;
            this.panel45.Location = new System.Drawing.Point(0, 30);
            this.panel45.Name = "panel45";
            this.panel45.Size = new System.Drawing.Size(674, 31);
            this.panel45.TabIndex = 24;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(10, 4);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(633, 24);
            this.label15.TabIndex = 29;
            this.label15.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel42
            // 
            this.panel42.BackColor = System.Drawing.SystemColors.Window;
            this.panel42.Controls.Add(this.label15);
            this.panel42.Controls.Add(this.panel43);
            this.panel42.Location = new System.Drawing.Point(2, 515);
            this.panel42.Name = "panel42";
            this.panel42.Size = new System.Drawing.Size(674, 31);
            this.panel42.TabIndex = 34;
            // 
            // panel41
            // 
            this.panel41.BackColor = System.Drawing.Color.Gray;
            this.panel41.Location = new System.Drawing.Point(0, 30);
            this.panel41.Name = "panel41";
            this.panel41.Size = new System.Drawing.Size(674, 31);
            this.panel41.TabIndex = 24;
            // 
            // panel40
            // 
            this.panel40.BackColor = System.Drawing.Color.Gray;
            this.panel40.Controls.Add(this.label17);
            this.panel40.Controls.Add(this.panel41);
            this.panel40.Location = new System.Drawing.Point(2, 545);
            this.panel40.Name = "panel40";
            this.panel40.Size = new System.Drawing.Size(674, 31);
            this.panel40.TabIndex = 35;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.Gray;
            this.panel13.Location = new System.Drawing.Point(0, 30);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(674, 31);
            this.panel13.TabIndex = 24;
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.SystemColors.Window;
            this.panel12.Controls.Add(this.label8);
            this.panel12.Controls.Add(this.panel13);
            this.panel12.Location = new System.Drawing.Point(2, 261);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(674, 31);
            this.panel12.TabIndex = 26;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(10, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(633, 24);
            this.label5.TabIndex = 29;
            this.label5.Text = "1.2.         Діагностика ходової частини                                         " +
    "                         200";
            // 
            // panel39
            // 
            this.panel39.BackColor = System.Drawing.Color.Gray;
            this.panel39.Controls.Add(this.panel50);
            this.panel39.Controls.Add(this.panel52);
            this.panel39.Location = new System.Drawing.Point(0, 30);
            this.panel39.Name = "panel39";
            this.panel39.Size = new System.Drawing.Size(674, 31);
            this.panel39.TabIndex = 24;
            // 
            // panel52
            // 
            this.panel52.BackColor = System.Drawing.Color.Gray;
            this.panel52.Controls.Add(this.panel61);
            this.panel52.Location = new System.Drawing.Point(0, 0);
            this.panel52.Name = "panel52";
            this.panel52.Size = new System.Drawing.Size(674, 31);
            this.panel52.TabIndex = 25;
            // 
            // panel61
            // 
            this.panel61.BackColor = System.Drawing.Color.Gray;
            this.panel61.Location = new System.Drawing.Point(0, 30);
            this.panel61.Name = "panel61";
            this.panel61.Size = new System.Drawing.Size(674, 31);
            this.panel61.TabIndex = 24;
            // 
            // panel50
            // 
            this.panel50.BackColor = System.Drawing.SystemColors.Window;
            this.panel50.Controls.Add(this.panel51);
            this.panel50.Location = new System.Drawing.Point(0, 30);
            this.panel50.Name = "panel50";
            this.panel50.Size = new System.Drawing.Size(674, 31);
            this.panel50.TabIndex = 26;
            // 
            // panel51
            // 
            this.panel51.BackColor = System.Drawing.Color.Gray;
            this.panel51.Location = new System.Drawing.Point(0, 30);
            this.panel51.Name = "panel51";
            this.panel51.Size = new System.Drawing.Size(674, 31);
            this.panel51.TabIndex = 24;
            // 
            // label22
            // 
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(10, 4);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(633, 24);
            this.label22.TabIndex = 29;
            this.label22.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel38
            // 
            this.panel38.BackColor = System.Drawing.SystemColors.Window;
            this.panel38.Controls.Add(this.label22);
            this.panel38.Controls.Add(this.panel39);
            this.panel38.Location = new System.Drawing.Point(2, 703);
            this.panel38.Name = "panel38";
            this.panel38.Size = new System.Drawing.Size(674, 31);
            this.panel38.TabIndex = 41;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(10, 4);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(633, 24);
            this.label19.TabIndex = 29;
            this.label19.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(205, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(271, 24);
            this.label18.TabIndex = 24;
            this.label18.Text = "2.Ремонт рульової рейки";
            // 
            // child_panel_second
            // 
            this.child_panel_second.Dock = System.Windows.Forms.DockStyle.Fill;
            this.child_panel_second.Location = new System.Drawing.Point(0, 0);
            this.child_panel_second.Name = "child_panel_second";
            this.child_panel_second.Size = new System.Drawing.Size(676, 764);
            this.child_panel_second.TabIndex = 46;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Black;
            this.panel3.ForeColor = System.Drawing.SystemColors.Window;
            this.panel3.Location = new System.Drawing.Point(2, 127);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(674, 8);
            this.panel3.TabIndex = 22;
            // 
            // panel16
            // 
            this.panel16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel16.Controls.Add(this.label9);
            this.panel16.ForeColor = System.Drawing.SystemColors.Window;
            this.panel16.Location = new System.Drawing.Point(2, 291);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(674, 37);
            this.panel16.TabIndex = 27;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Gray;
            this.panel5.Location = new System.Drawing.Point(0, 30);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(674, 31);
            this.panel5.TabIndex = 24;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(10, 4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(633, 24);
            this.label4.TabIndex = 25;
            this.label4.Text = "1.1.         Комп’ютерна діагностика                                             " +
    "                           200";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Gray;
            this.panel4.Controls.Add(this.label4);
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Location = new System.Drawing.Point(2, 171);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(674, 31);
            this.panel4.TabIndex = 23;
            // 
            // panel32
            // 
            this.panel32.BackColor = System.Drawing.Color.Gray;
            this.panel32.Location = new System.Drawing.Point(0, 30);
            this.panel32.Name = "panel32";
            this.panel32.Size = new System.Drawing.Size(674, 31);
            this.panel32.TabIndex = 24;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(10, 3);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(633, 24);
            this.label13.TabIndex = 25;
            this.label13.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // panel31
            // 
            this.panel31.BackColor = System.Drawing.Color.Gray;
            this.panel31.Controls.Add(this.label13);
            this.panel31.Controls.Add(this.panel32);
            this.panel31.Location = new System.Drawing.Point(2, 388);
            this.panel31.Name = "panel31";
            this.panel31.Size = new System.Drawing.Size(674, 31);
            this.panel31.TabIndex = 30;
            // 
            // panel15
            // 
            this.panel15.BackColor = System.Drawing.Color.Gray;
            this.panel15.Location = new System.Drawing.Point(0, 30);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(674, 31);
            this.panel15.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(10, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(633, 24);
            this.label6.TabIndex = 30;
            this.label6.Text = "1.3.         Діагностика гальмівної системи                                      " +
    "                      200";
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.Gray;
            this.panel14.Controls.Add(this.label6);
            this.panel14.Controls.Add(this.panel15);
            this.panel14.Location = new System.Drawing.Point(2, 231);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(674, 31);
            this.panel14.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(46, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 24);
            this.label1.TabIndex = 21;
            this.label1.Text = "Найменування";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.ForeColor = System.Drawing.SystemColors.Window;
            this.panel1.Location = new System.Drawing.Point(2, 91);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(674, 37);
            this.panel1.TabIndex = 20;
            // 
            // panel26
            // 
            this.panel26.BackColor = System.Drawing.Color.Gray;
            this.panel26.Controls.Add(this.panel27);
            this.panel26.Controls.Add(this.panel29);
            this.panel26.Location = new System.Drawing.Point(0, 30);
            this.panel26.Name = "panel26";
            this.panel26.Size = new System.Drawing.Size(674, 31);
            this.panel26.TabIndex = 24;
            // 
            // panel29
            // 
            this.panel29.BackColor = System.Drawing.Color.Gray;
            this.panel29.Controls.Add(this.panel30);
            this.panel29.Location = new System.Drawing.Point(0, 0);
            this.panel29.Name = "panel29";
            this.panel29.Size = new System.Drawing.Size(674, 31);
            this.panel29.TabIndex = 25;
            // 
            // panel30
            // 
            this.panel30.BackColor = System.Drawing.Color.Gray;
            this.panel30.Location = new System.Drawing.Point(0, 30);
            this.panel30.Name = "panel30";
            this.panel30.Size = new System.Drawing.Size(674, 31);
            this.panel30.TabIndex = 24;
            // 
            // panel27
            // 
            this.panel27.BackColor = System.Drawing.SystemColors.Window;
            this.panel27.Controls.Add(this.panel28);
            this.panel27.Location = new System.Drawing.Point(0, 30);
            this.panel27.Name = "panel27";
            this.panel27.Size = new System.Drawing.Size(674, 31);
            this.panel27.TabIndex = 26;
            // 
            // panel28
            // 
            this.panel28.BackColor = System.Drawing.Color.Gray;
            this.panel28.Location = new System.Drawing.Point(0, 30);
            this.panel28.Name = "panel28";
            this.panel28.Size = new System.Drawing.Size(674, 31);
            this.panel28.TabIndex = 24;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(10, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(633, 24);
            this.label12.TabIndex = 29;
            this.label12.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel25
            // 
            this.panel25.BackColor = System.Drawing.SystemColors.Window;
            this.panel25.Controls.Add(this.label12);
            this.panel25.Controls.Add(this.panel26);
            this.panel25.Location = new System.Drawing.Point(2, 418);
            this.panel25.Name = "panel25";
            this.panel25.Size = new System.Drawing.Size(674, 31);
            this.panel25.TabIndex = 31;
            // 
            // panel49
            // 
            this.panel49.BackColor = System.Drawing.Color.Gray;
            this.panel49.Location = new System.Drawing.Point(0, 30);
            this.panel49.Name = "panel49";
            this.panel49.Size = new System.Drawing.Size(674, 31);
            this.panel49.TabIndex = 24;
            // 
            // panel48
            // 
            this.panel48.BackColor = System.Drawing.Color.Gray;
            this.panel48.Controls.Add(this.label16);
            this.panel48.Controls.Add(this.panel49);
            this.panel48.Location = new System.Drawing.Point(2, 485);
            this.panel48.Name = "panel48";
            this.panel48.Size = new System.Drawing.Size(674, 31);
            this.panel48.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(495, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 24);
            this.label2.TabIndex = 23;
            this.label2.Text = "Ціна, грн*";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Repair_As_New.Properties.Resources._172529_price_usd_tag_icon__2_;
            this.pictureBox3.Location = new System.Drawing.Point(211, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(55, 55);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 19;
            this.pictureBox3.TabStop = false;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(248, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(174, 24);
            this.label3.TabIndex = 24;
            this.label3.Text = "1.Діагностика автомобіля";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.panel2.Controls.Add(this.label3);
            this.panel2.ForeColor = System.Drawing.SystemColors.Window;
            this.panel2.Location = new System.Drawing.Point(2, 134);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(674, 37);
            this.panel2.TabIndex = 21;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Repair_As_New.Properties.Resources._352532_looks_2_two_icon;
            this.pictureBox2.Location = new System.Drawing.Point(590, 9);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(55, 55);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 45;
            this.pictureBox2.TabStop = false;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(205, 7);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(271, 24);
            this.label9.TabIndex = 24;
            this.label9.Text = "2.Ремонт рульової рейки";
            // 
            // panel18
            // 
            this.panel18.BackColor = System.Drawing.Color.Gray;
            this.panel18.Controls.Add(this.panel19);
            this.panel18.Controls.Add(this.panel21);
            this.panel18.Location = new System.Drawing.Point(0, 30);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(674, 31);
            this.panel18.TabIndex = 24;
            // 
            // panel21
            // 
            this.panel21.BackColor = System.Drawing.Color.Gray;
            this.panel21.Controls.Add(this.panel22);
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(674, 31);
            this.panel21.TabIndex = 25;
            // 
            // panel22
            // 
            this.panel22.BackColor = System.Drawing.Color.Gray;
            this.panel22.Location = new System.Drawing.Point(0, 30);
            this.panel22.Name = "panel22";
            this.panel22.Size = new System.Drawing.Size(674, 31);
            this.panel22.TabIndex = 24;
            // 
            // panel19
            // 
            this.panel19.BackColor = System.Drawing.SystemColors.Window;
            this.panel19.Controls.Add(this.panel20);
            this.panel19.Location = new System.Drawing.Point(0, 30);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(674, 31);
            this.panel19.TabIndex = 26;
            // 
            // panel20
            // 
            this.panel20.BackColor = System.Drawing.Color.Gray;
            this.panel20.Location = new System.Drawing.Point(0, 30);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(674, 31);
            this.panel20.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(10, 4);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(633, 24);
            this.label11.TabIndex = 29;
            this.label11.Text = "1.2.         Реставрація гідравлічної кермової рейки                             " +
    "                                     200";
            // 
            // panel17
            // 
            this.panel17.BackColor = System.Drawing.SystemColors.Window;
            this.panel17.Controls.Add(this.label11);
            this.panel17.Controls.Add(this.panel18);
            this.panel17.Location = new System.Drawing.Point(2, 358);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(674, 31);
            this.panel17.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(205, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(271, 24);
            this.label14.TabIndex = 24;
            this.label14.Text = "2.Ремонт рульової рейки";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(10, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(633, 24);
            this.label8.TabIndex = 30;
            this.label8.Text = "1.1.         Комп’ютерна діагностика                                             " +
    "                           200";
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(10, 4);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(633, 24);
            this.label24.TabIndex = 25;
            this.label24.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(272, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 29);
            this.label7.TabIndex = 16;
            this.label7.Text = "Ціни";
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label17.Location = new System.Drawing.Point(10, 3);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(633, 24);
            this.label17.TabIndex = 25;
            this.label17.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(10, 4);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(633, 24);
            this.label20.TabIndex = 25;
            this.label20.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(10, 4);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(633, 24);
            this.label16.TabIndex = 25;
            this.label16.Text = "1.1.         Реставрація механічної кермової рейки                               " +
    "                                         200";
            // 
            // Prices_Form_II
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.AutoScroll = true;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(655, 758);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.panel36);
            this.Controls.Add(this.panel38);
            this.Controls.Add(this.panel62);
            this.Controls.Add(this.panel34);
            this.Controls.Add(this.panel53);
            this.Controls.Add(this.panel59);
            this.Controls.Add(this.panel33);
            this.Controls.Add(this.panel25);
            this.Controls.Add(this.panel31);
            this.Controls.Add(this.panel17);
            this.Controls.Add(this.panel48);
            this.Controls.Add(this.panel23);
            this.Controls.Add(this.panel42);
            this.Controls.Add(this.panel16);
            this.Controls.Add(this.panel40);
            this.Controls.Add(this.panel12);
            this.Controls.Add(this.panel14);
            this.Controls.Add(this.panel6);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.child_panel_second);
            this.Name = "Prices_Form_II";
            this.Text = "Prices_Form";
            this.Load += new System.EventHandler(this.Prices_Form_II_Load);
            this.panel23.ResumeLayout(false);
            this.panel62.ResumeLayout(false);
            this.panel33.ResumeLayout(false);
            this.panel34.ResumeLayout(false);
            this.panel54.ResumeLayout(false);
            this.panel57.ResumeLayout(false);
            this.panel55.ResumeLayout(false);
            this.panel53.ResumeLayout(false);
            this.panel36.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel59.ResumeLayout(false);
            this.panel43.ResumeLayout(false);
            this.panel46.ResumeLayout(false);
            this.panel44.ResumeLayout(false);
            this.panel42.ResumeLayout(false);
            this.panel40.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel39.ResumeLayout(false);
            this.panel52.ResumeLayout(false);
            this.panel50.ResumeLayout(false);
            this.panel38.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel31.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel26.ResumeLayout(false);
            this.panel29.ResumeLayout(false);
            this.panel27.ResumeLayout(false);
            this.panel25.ResumeLayout(false);
            this.panel48.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel18.ResumeLayout(false);
            this.panel21.ResumeLayout(false);
            this.panel19.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel24;
        private System.Windows.Forms.Panel panel23;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel63;
        private System.Windows.Forms.Panel panel62;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Panel panel33;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel34;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Panel panel54;
        private System.Windows.Forms.Panel panel55;
        private System.Windows.Forms.Panel panel56;
        private System.Windows.Forms.Panel panel57;
        private System.Windows.Forms.Panel panel58;
        private System.Windows.Forms.Panel panel53;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel37;
        private System.Windows.Forms.Panel panel36;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel60;
        private System.Windows.Forms.Panel panel59;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Panel panel43;
        private System.Windows.Forms.Panel panel44;
        private System.Windows.Forms.Panel panel45;
        private System.Windows.Forms.Panel panel46;
        private System.Windows.Forms.Panel panel47;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Panel panel42;
        private System.Windows.Forms.Panel panel41;
        private System.Windows.Forms.Panel panel40;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel39;
        private System.Windows.Forms.Panel panel50;
        private System.Windows.Forms.Panel panel51;
        private System.Windows.Forms.Panel panel52;
        private System.Windows.Forms.Panel panel61;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Panel panel38;
        private System.Windows.Forms.Panel child_panel_second;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel32;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Panel panel31;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel26;
        private System.Windows.Forms.Panel panel27;
        private System.Windows.Forms.Panel panel28;
        private System.Windows.Forms.Panel panel29;
        private System.Windows.Forms.Panel panel30;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel25;
        private System.Windows.Forms.Panel panel49;
        private System.Windows.Forms.Panel panel48;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Panel panel22;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Label label7;
    }
}