﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;

namespace Repair_As_New
{
    public partial class Prices_Form : Form
    {
        List<Panel> listPanel = new List<Panel>();
        int index;

        public Prices_Form()
        {
            InitializeComponent();
        }

        private void Prices_Form_Load(object sender, EventArgs e)
        {
            listPanel.Add(child_panel_first);
            listPanel.Add(child_panel_second);
            listPanel[0].BringToFront();

        }
        private void roundButton1_Click_1(object sender, EventArgs e)   //Вперед
        {
            //if (index < listPanel.Count - 1)
            child_panel_second.Show();
            listPanel[1].BringToFront();

        }

        private void roundButton2_Click(object sender, EventArgs e)  //Назад
        {
            child_panel_second.Hide();
            listPanel[0].BringToFront();
        }
    }
}
