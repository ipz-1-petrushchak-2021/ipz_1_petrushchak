﻿
namespace Repair_As_New
{
    partial class Order_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Order_panel1 = new System.Windows.Forms.Panel();
            this.comboBox_Info_CityChose = new System.Windows.Forms.ComboBox();
            this.textBox_Info_Time = new System.Windows.Forms.TextBox();
            this.comboBox_Info_PosluguChose = new System.Windows.Forms.ComboBox();
            this.textBox_Info_Date = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.textBox_Info_Problem = new System.Windows.Forms.TextBox();
            this.textBox_Info_CarDist = new System.Windows.Forms.TextBox();
            this.textBox_Info_CarYear = new System.Windows.Forms.TextBox();
            this.textBox_Info_Brand = new System.Windows.Forms.TextBox();
            this.textBox_Contact_Email = new System.Windows.Forms.TextBox();
            this.textBox_Contact_Number = new System.Windows.Forms.TextBox();
            this.textBox_Contact_Surname = new System.Windows.Forms.TextBox();
            this.textBox_Contact_Name = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.roundButton1 = new Repair_As_New.RoundButton();
            this.Order_panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Order_panel1
            // 
            this.Order_panel1.Controls.Add(this.comboBox_Info_CityChose);
            this.Order_panel1.Controls.Add(this.textBox_Info_Time);
            this.Order_panel1.Controls.Add(this.comboBox_Info_PosluguChose);
            this.Order_panel1.Controls.Add(this.textBox_Info_Date);
            this.Order_panel1.Controls.Add(this.pictureBox2);
            this.Order_panel1.Controls.Add(this.pictureBox1);
            this.Order_panel1.Controls.Add(this.roundButton1);
            this.Order_panel1.Controls.Add(this.textBox_Info_Problem);
            this.Order_panel1.Controls.Add(this.textBox_Info_CarDist);
            this.Order_panel1.Controls.Add(this.textBox_Info_CarYear);
            this.Order_panel1.Controls.Add(this.textBox_Info_Brand);
            this.Order_panel1.Controls.Add(this.textBox_Contact_Email);
            this.Order_panel1.Controls.Add(this.textBox_Contact_Number);
            this.Order_panel1.Controls.Add(this.textBox_Contact_Surname);
            this.Order_panel1.Controls.Add(this.textBox_Contact_Name);
            this.Order_panel1.Controls.Add(this.label7);
            this.Order_panel1.Controls.Add(this.label4);
            this.Order_panel1.Controls.Add(this.label1);
            this.Order_panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Order_panel1.Location = new System.Drawing.Point(0, 0);
            this.Order_panel1.Name = "Order_panel1";
            this.Order_panel1.Size = new System.Drawing.Size(657, 472);
            this.Order_panel1.TabIndex = 14;
            // 
            // comboBox_Info_CityChose
            // 
            this.comboBox_Info_CityChose.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBox_Info_CityChose.Font = new System.Drawing.Font("Arial", 15F);
            this.comboBox_Info_CityChose.FormattingEnabled = true;
            this.comboBox_Info_CityChose.Location = new System.Drawing.Point(346, 239);
            this.comboBox_Info_CityChose.Name = "comboBox_Info_CityChose";
            this.comboBox_Info_CityChose.Size = new System.Drawing.Size(268, 31);
            this.comboBox_Info_CityChose.TabIndex = 37;
            //this.comboBox_Info_CityChose.SelectedIndexChanged += new System.EventHandler(this.comboBox_Info_CityChose_SelectedIndexChanged);
            this.comboBox_Info_CityChose.Enter += new System.EventHandler(this.comboBox_Info_CityChose_Enter);
            this.comboBox_Info_CityChose.Leave += new System.EventHandler(this.comboBox_Info_CityChose_Leave);
            // 
            // textBox_Info_Time
            // 
            this.textBox_Info_Time.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Info_Time.Font = new System.Drawing.Font("Arial", 15F);
            this.textBox_Info_Time.Location = new System.Drawing.Point(346, 329);
            this.textBox_Info_Time.Name = "textBox_Info_Time";
            this.textBox_Info_Time.Size = new System.Drawing.Size(268, 30);
            this.textBox_Info_Time.TabIndex = 35;
            this.textBox_Info_Time.Enter += new System.EventHandler(this.textBox_Info_Time_Enter);
            this.textBox_Info_Time.Leave += new System.EventHandler(this.textBox_Info_Time_Leave);
            // 
            // comboBox_Info_PosluguChose
            // 
            this.comboBox_Info_PosluguChose.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.comboBox_Info_PosluguChose.Font = new System.Drawing.Font("Arial", 15F);
            this.comboBox_Info_PosluguChose.FormattingEnabled = true;
            this.comboBox_Info_PosluguChose.Location = new System.Drawing.Point(48, 239);
            this.comboBox_Info_PosluguChose.Name = "comboBox_Info_PosluguChose";
            this.comboBox_Info_PosluguChose.Size = new System.Drawing.Size(268, 31);
            this.comboBox_Info_PosluguChose.TabIndex = 36;
            this.comboBox_Info_PosluguChose.Enter += new System.EventHandler(this.comboBox_Info_PosluguChose_Enter);
            this.comboBox_Info_PosluguChose.Leave += new System.EventHandler(this.comboBox_Info_PosluguChose_Leave);
            // 
            // textBox_Info_Date
            // 
            this.textBox_Info_Date.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Info_Date.Font = new System.Drawing.Font("Arial", 15F);
            this.textBox_Info_Date.Location = new System.Drawing.Point(48, 329);
            this.textBox_Info_Date.Name = "textBox_Info_Date";
            this.textBox_Info_Date.Size = new System.Drawing.Size(268, 30);
            this.textBox_Info_Date.TabIndex = 34;
            this.textBox_Info_Date.Enter += new System.EventHandler(this.textBox_Info_Date_Enter);
            this.textBox_Info_Date.Leave += new System.EventHandler(this.textBox_Info_Date_Leave);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBox2.Image = global::Repair_As_New.Properties.Resources._2024660_call_contact_us_contacts_phone_support_icon;
            this.pictureBox2.Location = new System.Drawing.Point(268, 46);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(32, 32);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 33;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBox1.Image = global::Repair_As_New.Properties.Resources._3669162_info_ic_icon;
            this.pictureBox1.Location = new System.Drawing.Point(268, 194);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(32, 32);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 32;
            this.pictureBox1.TabStop = false;
            // 
            // textBox_Info_Problem
            // 
            this.textBox_Info_Problem.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Info_Problem.Font = new System.Drawing.Font("Arial", 15F);
            this.textBox_Info_Problem.Location = new System.Drawing.Point(48, 372);
            this.textBox_Info_Problem.Multiline = true;
            this.textBox_Info_Problem.Name = "textBox_Info_Problem";
            this.textBox_Info_Problem.Size = new System.Drawing.Size(566, 54);
            this.textBox_Info_Problem.TabIndex = 30;
            this.textBox_Info_Problem.Enter += new System.EventHandler(this.textBox_Info_Problem_Enter);
            this.textBox_Info_Problem.Leave += new System.EventHandler(this.textBox_Info_Problem_Leave);
            // 
            // textBox_Info_CarDist
            // 
            this.textBox_Info_CarDist.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Info_CarDist.Font = new System.Drawing.Font("Arial", 15F);
            this.textBox_Info_CarDist.Location = new System.Drawing.Point(480, 286);
            this.textBox_Info_CarDist.Name = "textBox_Info_CarDist";
            this.textBox_Info_CarDist.Size = new System.Drawing.Size(134, 30);
            this.textBox_Info_CarDist.TabIndex = 29;
            this.textBox_Info_CarDist.Enter += new System.EventHandler(this.textBox_Info_CarDist_Enter);
            this.textBox_Info_CarDist.Leave += new System.EventHandler(this.textBox_Info_CarDist_Leave);
            // 
            // textBox_Info_CarYear
            // 
            this.textBox_Info_CarYear.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Info_CarYear.Font = new System.Drawing.Font("Arial", 15F);
            this.textBox_Info_CarYear.Location = new System.Drawing.Point(346, 286);
            this.textBox_Info_CarYear.Name = "textBox_Info_CarYear";
            this.textBox_Info_CarYear.Size = new System.Drawing.Size(134, 30);
            this.textBox_Info_CarYear.TabIndex = 28;
            this.textBox_Info_CarYear.Enter += new System.EventHandler(this.textBox_Info_CarYear_Enter);
            this.textBox_Info_CarYear.Leave += new System.EventHandler(this.textBox_Info_CarYear_Leave);
            // 
            // textBox_Info_Brand
            // 
            this.textBox_Info_Brand.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Info_Brand.Font = new System.Drawing.Font("Arial", 15F);
            this.textBox_Info_Brand.Location = new System.Drawing.Point(48, 286);
            this.textBox_Info_Brand.Name = "textBox_Info_Brand";
            this.textBox_Info_Brand.Size = new System.Drawing.Size(268, 30);
            this.textBox_Info_Brand.TabIndex = 27;
            this.textBox_Info_Brand.Enter += new System.EventHandler(this.textBox_Info_Brand_Enter);
            this.textBox_Info_Brand.Leave += new System.EventHandler(this.textBox_Info_Brand_Leave);
            // 
            // textBox_Contact_Email
            // 
            this.textBox_Contact_Email.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Contact_Email.Font = new System.Drawing.Font("Arial", 15F);
            this.textBox_Contact_Email.Location = new System.Drawing.Point(346, 138);
            this.textBox_Contact_Email.Name = "textBox_Contact_Email";
            this.textBox_Contact_Email.Size = new System.Drawing.Size(268, 30);
            this.textBox_Contact_Email.TabIndex = 24;
            this.textBox_Contact_Email.Enter += new System.EventHandler(this.textBox_Contact_Email_Enter);
            this.textBox_Contact_Email.Leave += new System.EventHandler(this.textBox_Contact_Email_Leave);
            // 
            // textBox_Contact_Number
            // 
            this.textBox_Contact_Number.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Contact_Number.Font = new System.Drawing.Font("Arial", 15F);
            this.textBox_Contact_Number.Location = new System.Drawing.Point(48, 138);
            this.textBox_Contact_Number.Name = "textBox_Contact_Number";
            this.textBox_Contact_Number.Size = new System.Drawing.Size(268, 30);
            this.textBox_Contact_Number.TabIndex = 23;
            this.textBox_Contact_Number.Enter += new System.EventHandler(this.textBox_Contact_Number_Enter);
            this.textBox_Contact_Number.Leave += new System.EventHandler(this.textBox_Contact_Number_Leave);
            // 
            // textBox_Contact_Surname
            // 
            this.textBox_Contact_Surname.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Contact_Surname.Font = new System.Drawing.Font("Arial", 15F);
            this.textBox_Contact_Surname.Location = new System.Drawing.Point(346, 92);
            this.textBox_Contact_Surname.Name = "textBox_Contact_Surname";
            this.textBox_Contact_Surname.Size = new System.Drawing.Size(268, 30);
            this.textBox_Contact_Surname.TabIndex = 22;
            this.textBox_Contact_Surname.Enter += new System.EventHandler(this.textBox_Contact_Surname_Enter);
            this.textBox_Contact_Surname.Leave += new System.EventHandler(this.textBox_Contact_Surname_Leave);
            // 
            // textBox_Contact_Name
            // 
            this.textBox_Contact_Name.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.textBox_Contact_Name.Font = new System.Drawing.Font("Arial", 15F);
            this.textBox_Contact_Name.Location = new System.Drawing.Point(48, 92);
            this.textBox_Contact_Name.Name = "textBox_Contact_Name";
            this.textBox_Contact_Name.Size = new System.Drawing.Size(268, 30);
            this.textBox_Contact_Name.TabIndex = 21;
            this.textBox_Contact_Name.TextChanged += new System.EventHandler(this.textBox_Contact_Name_TextChanged);
            this.textBox_Contact_Name.Enter += new System.EventHandler(this.textBox_Contact_Name_Enter);
            this.textBox_Contact_Name.Leave += new System.EventHandler(this.textBox_Contact_Name_Leave);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(12, 194);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(228, 25);
            this.label7.TabIndex = 20;
            this.label7.Text = "Детальна інформація\r\n";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(236, 25);
            this.label4.TabIndex = 17;
            this.label4.Text = "Контактна інформація";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(168, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(322, 31);
            this.label1.TabIndex = 14;
            this.label1.Text = "Записатись на послугу";
            // 
            // roundButton1
            // 
            this.roundButton1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.roundButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.roundButton1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(58)))), ((int)(((byte)(12)))), ((int)(((byte)(163)))));
            this.roundButton1.BorderColor = System.Drawing.Color.PaleVioletRed;
            this.roundButton1.BorderRadius = 10;
            this.roundButton1.BorderSize = 0;
            this.roundButton1.FlatAppearance.BorderSize = 0;
            this.roundButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.roundButton1.ForeColor = System.Drawing.Color.White;
            this.roundButton1.Location = new System.Drawing.Point(251, 432);
            this.roundButton1.Name = "roundButton1";
            this.roundButton1.Size = new System.Drawing.Size(150, 40);
            this.roundButton1.TabIndex = 31;
            this.roundButton1.Text = "Надіслати";
            this.roundButton1.TextColor = System.Drawing.Color.White;
            this.roundButton1.UseVisualStyleBackColor = false;
            this.roundButton1.Click += new System.EventHandler(this.roundButton1_Click);
            // 
            // Order_Form
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(657, 472);
            this.Controls.Add(this.Order_panel1);
            this.Name = "Order_Form";
            this.Text = "Order_Form";
            this.Load += new System.EventHandler(this.Order_Form_Load);
            this.Order_panel1.ResumeLayout(false);
            this.Order_panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Order_panel1;
        private System.Windows.Forms.ComboBox comboBox_Info_PosluguChose;
        private System.Windows.Forms.TextBox textBox_Info_Time;
        private System.Windows.Forms.TextBox textBox_Info_Date;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private RoundButton roundButton1;
        private System.Windows.Forms.TextBox textBox_Info_Problem;
        private System.Windows.Forms.TextBox textBox_Info_CarDist;
        private System.Windows.Forms.TextBox textBox_Info_CarYear;
        private System.Windows.Forms.TextBox textBox_Info_Brand;
        private System.Windows.Forms.TextBox textBox_Contact_Email;
        private System.Windows.Forms.TextBox textBox_Contact_Number;
        private System.Windows.Forms.TextBox textBox_Contact_Name;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Contact_Surname;
        private System.Windows.Forms.ComboBox comboBox_Info_CityChose;
    }
}