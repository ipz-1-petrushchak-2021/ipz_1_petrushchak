﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
//using Newtonsoft.Json;

namespace Repair_As_New
{
    public partial class Order_Form : Form
    {
        private Socket senderSocket;
        private String loggedUserName;
        private DateTime now;
        public Order_Form(Socket senderSocket, String loggedUserName, DateTime now)
        {
            this.senderSocket = senderSocket;
            this.now = now;
            InitializeComponent();

            textBox_Contact_Name.Text = "Ваше ім'я";              // текстики для Конктактів
            textBox_Contact_Name.ForeColor = Color.Gray;                //написи для показування дефолтного 
            textBox_Contact_Surname.Text = "Ваше прізвище";             // напису в текстбоксах
            textBox_Contact_Surname.ForeColor = Color.Gray;
            textBox_Contact_Number.Text = "Номер телефону";
            textBox_Contact_Number.ForeColor = Color.Gray;
            textBox_Contact_Email.Text = "Email";
            textBox_Contact_Email.ForeColor = Color.Gray;

            comboBox_Info_CityChose.Text = "Виберіть місто";               // текстики для Інформації
            comboBox_Info_CityChose.ForeColor = Color.Gray;
            comboBox_Info_PosluguChose.Text = "Виберіть послугу";               // текстики для Інформації
            comboBox_Info_PosluguChose.ForeColor = Color.Gray;
            textBox_Info_Brand.Text = "Введіть марку авто";
            textBox_Info_Brand.ForeColor = Color.Gray;
            textBox_Info_Date.Text = "Введіть дату";
            textBox_Info_Date.ForeColor = Color.Gray;
            textBox_Info_Time.Text = "Введіть час";
            textBox_Info_Time.ForeColor = Color.Gray;
            textBox_Info_CarYear.Text = "Рік випуску";
            textBox_Info_CarYear.ForeColor = Color.Gray;
            textBox_Info_CarDist.Text = "Пробіг";
            textBox_Info_CarDist.ForeColor = Color.Gray;
            textBox_Info_Problem.Text = "Опишіть проблему";
            textBox_Info_Problem.ForeColor = Color.Gray;



        }
        #region Contacts_function
        //name function
        private void textBox_Contact_Name_Enter(object sender, EventArgs e)
        {
            if (textBox_Contact_Name.Text == "Ваше ім'я")
            {
                textBox_Contact_Name.Text = "";
                textBox_Contact_Name.ForeColor = Color.Black;
            }
        }
        private void textBox_Contact_Name_Leave(object sender, EventArgs e)
        {
            if (textBox_Contact_Name.Text == "")
            {
                textBox_Contact_Name.ForeColor = Color.Gray;
                textBox_Contact_Name.Text = "Ваше ім'я";


            }
        }
        //surname function
        private void textBox_Contact_Surname_Enter(object sender, EventArgs e)
        {
            if (textBox_Contact_Surname.Text == "Ваше прізвище")
            {
                textBox_Contact_Surname.Text = "";
                textBox_Contact_Surname.ForeColor = Color.Black;
            }
        }
        private void textBox_Contact_Surname_Leave(object sender, EventArgs e)
        {
            if (textBox_Contact_Surname.Text == "")
            {
                textBox_Contact_Surname.ForeColor = Color.Gray;
                textBox_Contact_Surname.Text = "Ваше прізвище";


            }
        }
        //phone function
        private void textBox_Contact_Number_Enter(object sender, EventArgs e)
        {
            if (textBox_Contact_Number.Text == "Номер телефону")
            {
                textBox_Contact_Number.Text = "";
                textBox_Contact_Number.ForeColor = Color.Black;
            }
        }
        private void textBox_Contact_Number_Leave(object sender, EventArgs e)
        {
            if (textBox_Contact_Number.Text == "")
            {
                textBox_Contact_Number.ForeColor = Color.Gray;
                textBox_Contact_Number.Text = "Номер телефону";


            }
        }
        //email function
        private void textBox_Contact_Email_Enter(object sender, EventArgs e)
        {
            if (textBox_Contact_Email.Text == "Email")
            {
                textBox_Contact_Email.Text = "";
                textBox_Contact_Email.ForeColor = Color.Black;
            }
        }
        private void textBox_Contact_Email_Leave(object sender, EventArgs e)
        {
            if (textBox_Contact_Email.Text == "")
            {
                textBox_Contact_Email.ForeColor = Color.Gray;
                textBox_Contact_Email.Text = "Email";


            }
        }
        #endregion Contacts_function
        #region Information_function
        private void comboBox_Info_CityChose_Enter(object sender, EventArgs e)
        {
            if (comboBox_Info_CityChose.Text == "Виберіть місто")
            {
                comboBox_Info_CityChose.Text = "";
                comboBox_Info_CityChose.ForeColor = Color.Black;

            }
        }
        private void comboBox_Info_CityChose_Leave(object sender, EventArgs e)
        {

            if (comboBox_Info_CityChose.Text == "")
            {
                comboBox_Info_CityChose.ForeColor = Color.Gray;
                comboBox_Info_CityChose.Text = "Виберіть місто";


            }

        }
        //Posluga function
        private void comboBox_Info_PosluguChose_Enter(object sender, EventArgs e)
        {
            if (comboBox_Info_PosluguChose.Text == "Виберіть послугу")
            {
                comboBox_Info_PosluguChose.Text = "";
                comboBox_Info_PosluguChose.ForeColor = Color.Black;
            }
        }
        private void comboBox_Info_PosluguChose_Leave(object sender, EventArgs e)
        {
            if (comboBox_Info_PosluguChose.Text == "")
            {
                comboBox_Info_PosluguChose.ForeColor = Color.Gray;
                comboBox_Info_PosluguChose.Text = "Виберіть послугу";
            

            }
        }
        //Car brand function
        private void textBox_Info_Brand_Enter(object sender, EventArgs e)
        {
            if (textBox_Info_Brand.Text == "Введіть марку авто")
            {
                textBox_Info_Brand.Text = "";
                textBox_Info_Brand.ForeColor = Color.Black;
            }
        }
        private void textBox_Info_Brand_Leave(object sender, EventArgs e)
        {
            if (textBox_Info_Brand.Text == "")
            {
                textBox_Info_Brand.ForeColor = Color.Gray;
                textBox_Info_Brand.Text = "Введіть марку авто";
            }
        }
        private void textBox_Info_Date_Enter(object sender, EventArgs e)
        {
            if (textBox_Info_Date.Text == "Введіть дату")
            {
                textBox_Info_Date.Text = "";
                textBox_Info_Date.ForeColor = Color.Black;
            }
        }
        private void textBox_Info_Date_Leave(object sender, EventArgs e)
        {
            if (textBox_Info_Date.Text == "")
            {
                textBox_Info_Date.ForeColor = Color.Gray;
                textBox_Info_Date.Text = "Введіть дату";
            }
        }
        private void textBox_Info_CarYear_Enter(object sender, EventArgs e)
        {
            if (textBox_Info_CarYear.Text == "Рік випуску")
            {
                textBox_Info_CarYear.Text = "";
                textBox_Info_CarYear.ForeColor = Color.Black;
            }
        }
        private void textBox_Info_CarYear_Leave(object sender, EventArgs e)
        {
            if (textBox_Info_CarYear.Text == "")
            {
                textBox_Info_CarYear.ForeColor = Color.Gray;
                textBox_Info_CarYear.Text = "Рік випуску";
            }
        }
        private void textBox_Info_CarDist_Enter(object sender, EventArgs e)
        {
            if (textBox_Info_CarDist.Text == "Пробіг")
            {
                textBox_Info_CarDist.Text = "";
                textBox_Info_CarDist.ForeColor = Color.Black;
            }
        }
        private void textBox_Info_CarDist_Leave(object sender, EventArgs e)
        {
            if (textBox_Info_CarDist.Text == "")
            {
                textBox_Info_CarDist.ForeColor = Color.Gray;
                textBox_Info_CarDist.Text = "Пробіг";
            }
        }
        private void textBox_Info_Time_Enter(object sender, EventArgs e)
        {
            if (textBox_Info_Time.Text == "Введіть час")
            {
                textBox_Info_Time.Text = "";
                textBox_Info_Time.ForeColor = Color.Black;
            }
        }
        private void textBox_Info_Time_Leave(object sender, EventArgs e)
        {
            if (textBox_Info_Time.Text == "")
            {
                textBox_Info_Time.ForeColor = Color.Gray;
                textBox_Info_Time.Text = "Введіть час";
            }
        }
        private void textBox_Info_Problem_Enter(object sender, EventArgs e)
        {
            if (textBox_Info_Problem.Text == "Опишіть проблему")
            {
                textBox_Info_Problem.Text = "";
                textBox_Info_Problem.ForeColor = Color.Black;
            }
        }
        private void textBox_Info_Problem_Leave(object sender, EventArgs e)
        {
            if (textBox_Info_Problem.Text == "")
            {
                textBox_Info_Problem.ForeColor = Color.Gray;
                textBox_Info_Problem.Text = "Опишіть проблему";
            }
        }
        #endregion

        private void textBox_Contact_Name_TextChanged(object sender, EventArgs e)
        {

        }
        //-------------------COMBO BOX FILLINGS

        private void roundButton1_Click(object sender, EventArgs e)  //button
        {
            if (textBox_Contact_Name.Text == "" || textBox_Contact_Number.Text == "" || comboBox_Info_PosluguChose.Text == "" || comboBox_Info_CityChose.Text == "" || textBox_Info_Date.Text == "")
            {
                MessageBox.Show("Вам потрібно обовязково заповнити імя, пошту, послугу, місто та вибрати дату.");
            }
            else
            {
                bool dataExists = true;
                //test_ipz.Program.sendData(senderSocket, $"select count(*) from UserInfo where Email like '{this.logintextBox.Text}'", now);
                Repair_As_New.Program.sendData(senderSocket, $"busy users_date like '{this.textBox_Info_Date.Text}'", now);
                String v2 = Repair_As_New.Program.receiveData(senderSocket, now);

                if (v2 == "pass")
                {
                    dataExists = false;
                }
                else
                {
                    dataExists = true;
                }

                if (!dataExists)
                {
                    Repair_As_New.Program.sendData(senderSocket,
                        $"exec records_registration  '{textBox_Contact_Name.Text}', '{textBox_Contact_Surname.Text}', " +
                        $"'{textBox_Contact_Number.Text}', '{textBox_Contact_Email.Text}', " +
                        $"'{comboBox_Info_PosluguChose.Text}', '{comboBox_Info_CityChose.Text}', " +
                        $"'{textBox_Info_Brand.Text}', '{textBox_Info_CarYear.Text}', " +
                        $"'{textBox_Info_CarDist.Text}', '{textBox_Info_Date.Text}', " +
                        $"'{textBox_Info_Time.Text}', '{textBox_Info_Problem.Text}'", now);
                    v2 = Repair_As_New.Program.receiveData(senderSocket, now);

                    if (v2 == "pass")
                    {
                        MessageBox.Show("Service registartion success!");
                        //Repair_As_New.Program.sendData(senderSocket, $"add onlineuser '{loginFIELD.Text} {Email_Field.Text}'", now);
                        this.Hide();
                        //Main_Form main_menu = new Main_Form(senderSocket, $"{loginFIELD.Text} {Email_Field.Text}", this.now);
                        //main_menu.Show();
                    }
                    if (v2 == "fail")
                    {
                        MessageBox.Show("Something went wrong!");
                        this.Hide();
                    }
                }
                else
                {
                    MessageBox.Show("At this time it is already busy choose another time!");
                }
            }
        }

        private void Order_Form_Load(object sender, EventArgs e)
        {


            Repair_As_New.Program.sendData(senderSocket,$"select service_name from ran_services", now);
            String v2 = Repair_As_New.Program.receiveData(senderSocket, now);

            string[] cities = v2.Split('?');
            foreach( var city in cities)
            {
                comboBox_Info_PosluguChose.Items.Add(city);
            }
            
           

           // comboBox_Info_PosluguChose.Text = v2;


            Repair_As_New.Program.sendData(senderSocket, $"select city_name from cities", now);
            String v1 = Repair_As_New.Program.receiveData(senderSocket, now);

            string[] servicess = v1.Split('?');
            foreach (var serv in servicess)
            {
                comboBox_Info_CityChose.Items.Add(serv);
            }
        }

        private void comboBox_Info_CityChose_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
