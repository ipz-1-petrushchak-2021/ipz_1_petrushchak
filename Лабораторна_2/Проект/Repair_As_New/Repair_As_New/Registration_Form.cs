﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Text.RegularExpressions;

namespace Repair_As_New
{
    public partial class Registration_Form : Form
    {
        private Socket senderSocket;
        private DateTime now;
        public Registration_Form(Socket senderSocket, DateTime now)
        {
            this.senderSocket = senderSocket;
            this.now = now;
            InitializeComponent();
            #region textbox_beforehand_texts
            this.passwordFIELD.AutoSize = false;
            this.passwordFIELD.Size = new Size(this.passwordFIELD.Size.Width, 34);

            this.passwordFIELD2.AutoSize = false;
            this.passwordFIELD2.Size = new Size(this.passwordFIELD2.Size.Width, 34);

            Email_Field.Text = "Введіть пошту";
            Email_Field.ForeColor = Color.Gray;

            textBox1.Text = "Введіть номер";
            textBox1.ForeColor = Color.Gray;
            loginFIELD.Text = "Введіть логін";
            loginFIELD.ForeColor = Color.Gray;


            passwordFIELD.UseSystemPasswordChar = false;
            passwordFIELD.Text = "Введіть пароль";
            passwordFIELD.ForeColor = Color.Gray;
            passwordFIELD2.UseSystemPasswordChar = false;
            passwordFIELD2.Text = "Повторіть пароль";
            passwordFIELD2.ForeColor = Color.Gray;
            #endregion textbox_beforehand_texts
        }

        #region Fields_code
        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Repair_As_New.Program.sendData(senderSocket, $"closed", now);
            this.Close();
            Application.Exit();
        }

        private void passwordFIELD_TextChanged(object sender, EventArgs e)
        {

        }

        private void passwordFIELD2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Email_Field_Enter(object sender, EventArgs e)
        {
            if(Email_Field.Text == "Введіть пошту") 
            { 
            Email_Field.Text = "";
            Email_Field.ForeColor = Color.Black;
            }
        }

        private void Email_Field_Leave(object sender, EventArgs e)
        {
            if (Email_Field.Text == "")
            {
                Email_Field.ForeColor = Color.Gray;
                Email_Field.Text = "Введіть пошту";

               
            }
        }

        private void textBox1_Enter(object sender, EventArgs e)
        {
            if (textBox1.Text == "Введіть номер")
            {
                textBox1.Text = "";
                textBox1.ForeColor = Color.Black;
            }
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                textBox1.ForeColor = Color.Gray;
                textBox1.Text = "Введіть номер";
            }
        }

        private void loginFIELD_Enter(object sender, EventArgs e)
        {
            if (loginFIELD.Text == "Введіть логін")
            {
                loginFIELD.Text = "";
                loginFIELD.ForeColor = Color.Black;
            }
        }

        private void loginFIELD_Leave(object sender, EventArgs e)
        {
            if (loginFIELD.Text == "")
            {
                loginFIELD.ForeColor = Color.Gray;
                loginFIELD.Text = "Введіть логін";
            }
        }

        private void passwordFIELD_Enter(object sender, EventArgs e)
        {
            if (passwordFIELD.Text == "Введіть пароль")
            {
                passwordFIELD.Text = "";
                passwordFIELD.UseSystemPasswordChar = true;
                passwordFIELD.ForeColor = Color.Black;
            }
        }

        private void passwordFIELD_Leave(object sender, EventArgs e)
        {
            if (passwordFIELD.Text == "")
            {
                passwordFIELD.UseSystemPasswordChar = false;
                passwordFIELD.ForeColor = Color.Gray;
                passwordFIELD.Text = "Введіть пароль";
            }
        }

        private void passwordFIELD2_Enter(object sender, EventArgs e)
        {
            if (passwordFIELD2.Text == "Повторіть пароль")
            {
                passwordFIELD2.Text = "";
                passwordFIELD2.UseSystemPasswordChar = true;
                passwordFIELD2.ForeColor = Color.Black;
            }
        }

        private void passwordFIELD2_Leave(object sender, EventArgs e)
        {
            if (passwordFIELD2.Text == "")
            {
                passwordFIELD2.UseSystemPasswordChar = false;
                passwordFIELD2.ForeColor = Color.Gray;
                passwordFIELD2.Text = "Повторіть пароль";
            }
        }

        Point lastPoint;
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void New_Exit_Button_Click(object sender, EventArgs e)
        {
            Repair_As_New.Program.sendData(senderSocket, $"closed", now);
            this.Close();
            Application.Exit();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }



        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Login_Form registering = new Login_Form(this.senderSocket, this.now);
            registering.Show();
        }
        #endregion Fields_code

        private void roundButton1_Click(object sender, EventArgs e)
        {
            if (Email_Field.Text == "" || loginFIELD.Text == "" || passwordFIELD.Text == "" || passwordFIELD2.Text == "")
            {
                MessageBox.Show("Вам потрібно заповнити пошту, логін та підтвердити пароль.");
            }
            else if(passwordFIELD.Text != passwordFIELD2.Text)
            {
                MessageBox.Show("Підтвердіть будь ласка пароль.");
            }
            else 
            {
                if (loginFIELD.Text != "")
                {
                    bool dataExists = true;
                    //test_ipz.Program.sendData(senderSocket, $"select count(*) from UserInfo where Email like '{this.logintextBox.Text}'", now);
                    Repair_As_New.Program.sendData(senderSocket, $"check user_login like '{this.loginFIELD.Text}'", now);
                    String v1 = Repair_As_New.Program.receiveData(senderSocket, now);

                    if (v1 == "pass")
                    {
                        dataExists = false;
                    }
                    else
                    {
                        dataExists = true;
                    }

                    if (!dataExists)
                    {
                        Repair_As_New.Program.sendData(senderSocket,
                            $"exec user_registration  '{Email_Field.Text}', '{textBox1.Text}', '{loginFIELD.Text}', '{passwordFIELD.Text}'", now);
                        v1 = Repair_As_New.Program.receiveData(senderSocket, now);

                        if (v1 == "pass")
                        {
                            MessageBox.Show("Registation success!");
                            Repair_As_New.Program.sendData(senderSocket, $"add onlineuser '{loginFIELD.Text} {Email_Field.Text}'", now);
                            this.Hide();
                            Main_Form main_menu = new Main_Form(senderSocket, $"{loginFIELD.Text} {Email_Field.Text}", this.now);
                            main_menu.Show();
                        }
                    }
                    else
                    {
                        MessageBox.Show("login already exists!");
                    }
                    //some code
                    Clear();            //maybe can cause some problems)
                }
                else
                {
                    MessageBox.Show("Enter a correct login");
                    loginFIELD.Text = "";
                }
            }
        }

        void Clear()
        {
            Email_Field.Text = "";
            textBox1.Text = "";
            loginFIELD.Text = "";
            passwordFIELD.Text = "";
            passwordFIELD2.Text = "";
        }

        private void Email_Field_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
