﻿
namespace Repair_As_New
{
    partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
            this.panel_SideMenu = new System.Windows.Forms.Panel();
            this.subpanelContacts = new System.Windows.Forms.Panel();
            this.subbuttonContacts3 = new System.Windows.Forms.Button();
            this.subbuttonContacts2 = new System.Windows.Forms.Button();
            this.subbuttonContacts1 = new System.Windows.Forms.Button();
            this.buttonContacts = new System.Windows.Forms.Button();
            this.buttonOrders = new System.Windows.Forms.Button();
            this.buttonCost = new System.Windows.Forms.Button();
            this.subpanelNews = new System.Windows.Forms.Panel();
            this.subbuttonNews3 = new System.Windows.Forms.Button();
            this.subbuttonNews2 = new System.Windows.Forms.Button();
            this.buttonNews = new System.Windows.Forms.Button();
            this.subpanelPoslugu = new System.Windows.Forms.Panel();
            this.subbuttonPoslugu6 = new System.Windows.Forms.Button();
            this.subbuttonPoslugu5 = new System.Windows.Forms.Button();
            this.subbuttonPoslugu4 = new System.Windows.Forms.Button();
            this.subbuttonPoslugu3 = new System.Windows.Forms.Button();
            this.subbuttonPoslugu2 = new System.Windows.Forms.Button();
            this.subbuttonPoslugu1 = new System.Windows.Forms.Button();
            this.buttonPoslugu = new System.Windows.Forms.Button();
            this.PanelLogo = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panelChildForm = new System.Windows.Forms.Panel();
            this.New_Exit_Button = new Repair_As_New.RoundButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel_SideMenu.SuspendLayout();
            this.subpanelContacts.SuspendLayout();
            this.subpanelNews.SuspendLayout();
            this.subpanelPoslugu.SuspendLayout();
            this.PanelLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panelChildForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_SideMenu
            // 
            this.panel_SideMenu.AutoScroll = true;
            this.panel_SideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.panel_SideMenu.Controls.Add(this.subpanelContacts);
            this.panel_SideMenu.Controls.Add(this.buttonContacts);
            this.panel_SideMenu.Controls.Add(this.buttonOrders);
            this.panel_SideMenu.Controls.Add(this.buttonCost);
            this.panel_SideMenu.Controls.Add(this.subpanelNews);
            this.panel_SideMenu.Controls.Add(this.buttonNews);
            this.panel_SideMenu.Controls.Add(this.subpanelPoslugu);
            this.panel_SideMenu.Controls.Add(this.buttonPoslugu);
            this.panel_SideMenu.Controls.Add(this.PanelLogo);
            this.panel_SideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_SideMenu.Location = new System.Drawing.Point(0, 0);
            this.panel_SideMenu.Name = "panel_SideMenu";
            this.panel_SideMenu.Size = new System.Drawing.Size(218, 511);
            this.panel_SideMenu.TabIndex = 0;
            this.panel_SideMenu.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_SideMenu_Paint);
            // 
            // subpanelContacts
            // 
            this.subpanelContacts.AutoScroll = true;
            this.subpanelContacts.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(218)))), ((int)(((byte)(220)))));
            this.subpanelContacts.Controls.Add(this.subbuttonContacts3);
            this.subpanelContacts.Controls.Add(this.subbuttonContacts2);
            this.subpanelContacts.Controls.Add(this.subbuttonContacts1);
            this.subpanelContacts.Dock = System.Windows.Forms.DockStyle.Top;
            this.subpanelContacts.Location = new System.Drawing.Point(0, 660);
            this.subpanelContacts.Name = "subpanelContacts";
            this.subpanelContacts.Size = new System.Drawing.Size(201, 128);
            this.subpanelContacts.TabIndex = 26;
            // 
            // subbuttonContacts3
            // 
            this.subbuttonContacts3.Dock = System.Windows.Forms.DockStyle.Top;
            this.subbuttonContacts3.FlatAppearance.BorderSize = 0;
            this.subbuttonContacts3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.subbuttonContacts3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subbuttonContacts3.Location = new System.Drawing.Point(0, 80);
            this.subbuttonContacts3.Name = "subbuttonContacts3";
            this.subbuttonContacts3.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.subbuttonContacts3.Size = new System.Drawing.Size(201, 40);
            this.subbuttonContacts3.TabIndex = 2;
            this.subbuttonContacts3.Text = "Головний офіс та служби підтримки";
            this.subbuttonContacts3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subbuttonContacts3.UseVisualStyleBackColor = true;
            this.subbuttonContacts3.Click += new System.EventHandler(this.subbuttonContacts3_Click);
            // 
            // subbuttonContacts2
            // 
            this.subbuttonContacts2.Dock = System.Windows.Forms.DockStyle.Top;
            this.subbuttonContacts2.FlatAppearance.BorderSize = 0;
            this.subbuttonContacts2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.subbuttonContacts2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subbuttonContacts2.Location = new System.Drawing.Point(0, 40);
            this.subbuttonContacts2.Name = "subbuttonContacts2";
            this.subbuttonContacts2.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.subbuttonContacts2.Size = new System.Drawing.Size(201, 40);
            this.subbuttonContacts2.TabIndex = 1;
            this.subbuttonContacts2.Text = "Пошта та месенджери";
            this.subbuttonContacts2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subbuttonContacts2.UseVisualStyleBackColor = true;
            this.subbuttonContacts2.Click += new System.EventHandler(this.subbuttonContacts2_Click);
            // 
            // subbuttonContacts1
            // 
            this.subbuttonContacts1.Dock = System.Windows.Forms.DockStyle.Top;
            this.subbuttonContacts1.FlatAppearance.BorderSize = 0;
            this.subbuttonContacts1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.subbuttonContacts1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subbuttonContacts1.Location = new System.Drawing.Point(0, 0);
            this.subbuttonContacts1.Name = "subbuttonContacts1";
            this.subbuttonContacts1.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.subbuttonContacts1.Size = new System.Drawing.Size(201, 40);
            this.subbuttonContacts1.TabIndex = 0;
            this.subbuttonContacts1.Text = "Гаряча лінія";
            this.subbuttonContacts1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subbuttonContacts1.UseVisualStyleBackColor = true;
            this.subbuttonContacts1.Click += new System.EventHandler(this.subbuttonContacts1_Click);
            // 
            // buttonContacts
            // 
            this.buttonContacts.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonContacts.FlatAppearance.BorderSize = 0;
            this.buttonContacts.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.buttonContacts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonContacts.Image = global::Repair_As_New.Properties.Resources._216165_contacts_icon__1_;
            this.buttonContacts.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonContacts.Location = new System.Drawing.Point(0, 615);
            this.buttonContacts.Name = "buttonContacts";
            this.buttonContacts.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.buttonContacts.Size = new System.Drawing.Size(201, 45);
            this.buttonContacts.TabIndex = 25;
            this.buttonContacts.Text = "Контакти";
            this.buttonContacts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonContacts.UseVisualStyleBackColor = true;
            this.buttonContacts.Click += new System.EventHandler(this.buttonContacts_Click);
            // 
            // buttonOrders
            // 
            this.buttonOrders.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonOrders.FlatAppearance.BorderSize = 0;
            this.buttonOrders.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.buttonOrders.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOrders.Image = global::Repair_As_New.Properties.Resources._352066_android_phone_icon__1_;
            this.buttonOrders.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonOrders.Location = new System.Drawing.Point(0, 570);
            this.buttonOrders.Name = "buttonOrders";
            this.buttonOrders.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.buttonOrders.Size = new System.Drawing.Size(201, 45);
            this.buttonOrders.TabIndex = 24;
            this.buttonOrders.Text = "Онлайн замовлення";
            this.buttonOrders.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonOrders.UseVisualStyleBackColor = true;
            this.buttonOrders.Click += new System.EventHandler(this.buttonOrders_Click);
            // 
            // buttonCost
            // 
            this.buttonCost.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonCost.FlatAppearance.BorderSize = 0;
            this.buttonCost.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.buttonCost.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCost.Image = global::Repair_As_New.Properties.Resources._3994417_cash_money_purchase_purse_wallet_icon__1_;
            this.buttonCost.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonCost.Location = new System.Drawing.Point(0, 525);
            this.buttonCost.Name = "buttonCost";
            this.buttonCost.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.buttonCost.Size = new System.Drawing.Size(201, 45);
            this.buttonCost.TabIndex = 23;
            this.buttonCost.Text = "Ціни";
            this.buttonCost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonCost.UseVisualStyleBackColor = true;
            this.buttonCost.Click += new System.EventHandler(this.buttonCost_Click);
            // 
            // subpanelNews
            // 
            this.subpanelNews.AutoScroll = true;
            this.subpanelNews.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(218)))), ((int)(((byte)(220)))));
            this.subpanelNews.Controls.Add(this.subbuttonNews3);
            this.subpanelNews.Controls.Add(this.subbuttonNews2);
            this.subpanelNews.Dock = System.Windows.Forms.DockStyle.Top;
            this.subpanelNews.Location = new System.Drawing.Point(0, 437);
            this.subpanelNews.Name = "subpanelNews";
            this.subpanelNews.Size = new System.Drawing.Size(201, 88);
            this.subpanelNews.TabIndex = 22;
            // 
            // subbuttonNews3
            // 
            this.subbuttonNews3.Dock = System.Windows.Forms.DockStyle.Top;
            this.subbuttonNews3.FlatAppearance.BorderSize = 0;
            this.subbuttonNews3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.subbuttonNews3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subbuttonNews3.Location = new System.Drawing.Point(0, 40);
            this.subbuttonNews3.Name = "subbuttonNews3";
            this.subbuttonNews3.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.subbuttonNews3.Size = new System.Drawing.Size(201, 40);
            this.subbuttonNews3.TabIndex = 2;
            this.subbuttonNews3.Text = "Новини СТО";
            this.subbuttonNews3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subbuttonNews3.UseVisualStyleBackColor = true;
            this.subbuttonNews3.Click += new System.EventHandler(this.subbuttonNews3_Click);
            // 
            // subbuttonNews2
            // 
            this.subbuttonNews2.Dock = System.Windows.Forms.DockStyle.Top;
            this.subbuttonNews2.FlatAppearance.BorderSize = 0;
            this.subbuttonNews2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.subbuttonNews2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subbuttonNews2.Location = new System.Drawing.Point(0, 0);
            this.subbuttonNews2.Name = "subbuttonNews2";
            this.subbuttonNews2.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.subbuttonNews2.Size = new System.Drawing.Size(201, 40);
            this.subbuttonNews2.TabIndex = 1;
            this.subbuttonNews2.Text = "Новини компанії";
            this.subbuttonNews2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subbuttonNews2.UseVisualStyleBackColor = true;
            this.subbuttonNews2.Click += new System.EventHandler(this.subbuttonNews2_Click);
            // 
            // buttonNews
            // 
            this.buttonNews.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonNews.FlatAppearance.BorderSize = 0;
            this.buttonNews.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.buttonNews.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNews.Image = global::Repair_As_New.Properties.Resources._8103339_office_megaphone_speaker_loudspeaker_announce_icon__2_;
            this.buttonNews.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonNews.Location = new System.Drawing.Point(0, 392);
            this.buttonNews.Name = "buttonNews";
            this.buttonNews.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.buttonNews.Size = new System.Drawing.Size(201, 45);
            this.buttonNews.TabIndex = 21;
            this.buttonNews.Text = "Новини";
            this.buttonNews.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonNews.UseVisualStyleBackColor = true;
            this.buttonNews.Click += new System.EventHandler(this.buttonNews_Click);
            // 
            // subpanelPoslugu
            // 
            this.subpanelPoslugu.AutoScroll = true;
            this.subpanelPoslugu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(218)))), ((int)(((byte)(220)))));
            this.subpanelPoslugu.Controls.Add(this.subbuttonPoslugu6);
            this.subpanelPoslugu.Controls.Add(this.subbuttonPoslugu5);
            this.subpanelPoslugu.Controls.Add(this.subbuttonPoslugu4);
            this.subpanelPoslugu.Controls.Add(this.subbuttonPoslugu3);
            this.subpanelPoslugu.Controls.Add(this.subbuttonPoslugu2);
            this.subpanelPoslugu.Controls.Add(this.subbuttonPoslugu1);
            this.subpanelPoslugu.Dock = System.Windows.Forms.DockStyle.Top;
            this.subpanelPoslugu.Location = new System.Drawing.Point(0, 145);
            this.subpanelPoslugu.Name = "subpanelPoslugu";
            this.subpanelPoslugu.Size = new System.Drawing.Size(201, 247);
            this.subpanelPoslugu.TabIndex = 2;
            this.subpanelPoslugu.Paint += new System.Windows.Forms.PaintEventHandler(this.subpanelPoslugu_Paint);
            // 
            // subbuttonPoslugu6
            // 
            this.subbuttonPoslugu6.Dock = System.Windows.Forms.DockStyle.Top;
            this.subbuttonPoslugu6.FlatAppearance.BorderSize = 0;
            this.subbuttonPoslugu6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.subbuttonPoslugu6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subbuttonPoslugu6.Image = global::Repair_As_New.Properties.Resources.transmissiya_1632236187840;
            this.subbuttonPoslugu6.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.subbuttonPoslugu6.Location = new System.Drawing.Point(0, 200);
            this.subbuttonPoslugu6.Name = "subbuttonPoslugu6";
            this.subbuttonPoslugu6.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.subbuttonPoslugu6.Size = new System.Drawing.Size(201, 40);
            this.subbuttonPoslugu6.TabIndex = 5;
            this.subbuttonPoslugu6.Text = "Ремонт КПП";
            this.subbuttonPoslugu6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subbuttonPoslugu6.UseVisualStyleBackColor = true;
            this.subbuttonPoslugu6.Click += new System.EventHandler(this.subbuttonPoslugu6_Click);
            // 
            // subbuttonPoslugu5
            // 
            this.subbuttonPoslugu5.Dock = System.Windows.Forms.DockStyle.Top;
            this.subbuttonPoslugu5.FlatAppearance.BorderSize = 0;
            this.subbuttonPoslugu5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.subbuttonPoslugu5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subbuttonPoslugu5.Image = global::Repair_As_New.Properties.Resources.dvigatel_1632236012714;
            this.subbuttonPoslugu5.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.subbuttonPoslugu5.Location = new System.Drawing.Point(0, 160);
            this.subbuttonPoslugu5.Name = "subbuttonPoslugu5";
            this.subbuttonPoslugu5.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.subbuttonPoslugu5.Size = new System.Drawing.Size(201, 40);
            this.subbuttonPoslugu5.TabIndex = 4;
            this.subbuttonPoslugu5.Text = "Ремонт двигуна";
            this.subbuttonPoslugu5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subbuttonPoslugu5.UseVisualStyleBackColor = true;
            this.subbuttonPoslugu5.Click += new System.EventHandler(this.subbuttonPoslugu5_Click);
            // 
            // subbuttonPoslugu4
            // 
            this.subbuttonPoslugu4.Dock = System.Windows.Forms.DockStyle.Top;
            this.subbuttonPoslugu4.FlatAppearance.BorderSize = 0;
            this.subbuttonPoslugu4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.subbuttonPoslugu4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subbuttonPoslugu4.Image = global::Repair_As_New.Properties.Resources.hodovaya_chast;
            this.subbuttonPoslugu4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.subbuttonPoslugu4.Location = new System.Drawing.Point(0, 120);
            this.subbuttonPoslugu4.Name = "subbuttonPoslugu4";
            this.subbuttonPoslugu4.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.subbuttonPoslugu4.Size = new System.Drawing.Size(201, 40);
            this.subbuttonPoslugu4.TabIndex = 3;
            this.subbuttonPoslugu4.Text = "Ремонт ходової";
            this.subbuttonPoslugu4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subbuttonPoslugu4.UseVisualStyleBackColor = true;
            this.subbuttonPoslugu4.Click += new System.EventHandler(this.subbuttonPoslugu4_Click);
            // 
            // subbuttonPoslugu3
            // 
            this.subbuttonPoslugu3.Dock = System.Windows.Forms.DockStyle.Top;
            this.subbuttonPoslugu3.FlatAppearance.BorderSize = 0;
            this.subbuttonPoslugu3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.subbuttonPoslugu3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subbuttonPoslugu3.Image = global::Repair_As_New.Properties.Resources.toplivnaya_sistema;
            this.subbuttonPoslugu3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.subbuttonPoslugu3.Location = new System.Drawing.Point(0, 80);
            this.subbuttonPoslugu3.Name = "subbuttonPoslugu3";
            this.subbuttonPoslugu3.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.subbuttonPoslugu3.Size = new System.Drawing.Size(201, 40);
            this.subbuttonPoslugu3.TabIndex = 2;
            this.subbuttonPoslugu3.Text = "Ремонт паливної апаратури";
            this.subbuttonPoslugu3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subbuttonPoslugu3.UseVisualStyleBackColor = true;
            this.subbuttonPoslugu3.Click += new System.EventHandler(this.subbuttonPoslugu3_Click);
            // 
            // subbuttonPoslugu2
            // 
            this.subbuttonPoslugu2.Dock = System.Windows.Forms.DockStyle.Top;
            this.subbuttonPoslugu2.FlatAppearance.BorderSize = 0;
            this.subbuttonPoslugu2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.subbuttonPoslugu2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subbuttonPoslugu2.Image = global::Repair_As_New.Properties.Resources.rulevoe_upravlenie;
            this.subbuttonPoslugu2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.subbuttonPoslugu2.Location = new System.Drawing.Point(0, 40);
            this.subbuttonPoslugu2.Name = "subbuttonPoslugu2";
            this.subbuttonPoslugu2.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.subbuttonPoslugu2.Size = new System.Drawing.Size(201, 40);
            this.subbuttonPoslugu2.TabIndex = 1;
            this.subbuttonPoslugu2.Text = "Ремонт рульової рейки";
            this.subbuttonPoslugu2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subbuttonPoslugu2.UseVisualStyleBackColor = true;
            this.subbuttonPoslugu2.Click += new System.EventHandler(this.subbuttonPoslugu2_Click_1);
            // 
            // subbuttonPoslugu1
            // 
            this.subbuttonPoslugu1.Dock = System.Windows.Forms.DockStyle.Top;
            this.subbuttonPoslugu1.FlatAppearance.BorderSize = 0;
            this.subbuttonPoslugu1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.subbuttonPoslugu1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.subbuttonPoslugu1.Image = global::Repair_As_New.Properties.Resources.diagnostika;
            this.subbuttonPoslugu1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.subbuttonPoslugu1.Location = new System.Drawing.Point(0, 0);
            this.subbuttonPoslugu1.Name = "subbuttonPoslugu1";
            this.subbuttonPoslugu1.Padding = new System.Windows.Forms.Padding(45, 0, 0, 0);
            this.subbuttonPoslugu1.Size = new System.Drawing.Size(201, 40);
            this.subbuttonPoslugu1.TabIndex = 0;
            this.subbuttonPoslugu1.Text = "Діагностика";
            this.subbuttonPoslugu1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.subbuttonPoslugu1.UseVisualStyleBackColor = true;
            this.subbuttonPoslugu1.Click += new System.EventHandler(this.subbuttonPoslugu1_Click_1);
            // 
            // buttonPoslugu
            // 
            this.buttonPoslugu.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonPoslugu.FlatAppearance.BorderSize = 0;
            this.buttonPoslugu.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(232)))), ((int)(((byte)(244)))));
            this.buttonPoslugu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPoslugu.Image = global::Repair_As_New.Properties.Resources._6875984_auto_car_repair_service_icon__1_;
            this.buttonPoslugu.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonPoslugu.Location = new System.Drawing.Point(0, 100);
            this.buttonPoslugu.Name = "buttonPoslugu";
            this.buttonPoslugu.Padding = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.buttonPoslugu.Size = new System.Drawing.Size(201, 45);
            this.buttonPoslugu.TabIndex = 1;
            this.buttonPoslugu.Text = "Послуги";
            this.buttonPoslugu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPoslugu.UseVisualStyleBackColor = true;
            this.buttonPoslugu.Click += new System.EventHandler(this.buttonPoslugu_Click);
            // 
            // PanelLogo
            // 
            this.PanelLogo.AutoScroll = true;
            this.PanelLogo.Controls.Add(this.pictureBox2);
            this.PanelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelLogo.Location = new System.Drawing.Point(0, 0);
            this.PanelLogo.Name = "PanelLogo";
            this.PanelLogo.Size = new System.Drawing.Size(201, 100);
            this.PanelLogo.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Repair_As_New.Properties.Resources.RAN_logos_blacfk__2_;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(201, 100);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // panelChildForm
            // 
            this.panelChildForm.BackColor = System.Drawing.SystemColors.Window;
            this.panelChildForm.Controls.Add(this.New_Exit_Button);
            this.panelChildForm.Controls.Add(this.pictureBox1);
            this.panelChildForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelChildForm.Location = new System.Drawing.Point(218, 0);
            this.panelChildForm.Name = "panelChildForm";
            this.panelChildForm.Size = new System.Drawing.Size(673, 511);
            this.panelChildForm.TabIndex = 1;
            this.panelChildForm.Paint += new System.Windows.Forms.PaintEventHandler(this.panelChildForm_Paint);
            // 
            // New_Exit_Button
            // 
            this.New_Exit_Button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(218)))), ((int)(((byte)(220)))));
            this.New_Exit_Button.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(218)))), ((int)(((byte)(220)))));
            this.New_Exit_Button.BorderColor = System.Drawing.Color.PaleVioletRed;
            this.New_Exit_Button.BorderRadius = 10;
            this.New_Exit_Button.BorderSize = 0;
            this.New_Exit_Button.FlatAppearance.BorderSize = 0;
            this.New_Exit_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.New_Exit_Button.Font = new System.Drawing.Font("Arial Narrow", 15.25F, System.Drawing.FontStyle.Bold);
            this.New_Exit_Button.ForeColor = System.Drawing.Color.Black;
            this.New_Exit_Button.Location = new System.Drawing.Point(495, 467);
            this.New_Exit_Button.Name = "New_Exit_Button";
            this.New_Exit_Button.Size = new System.Drawing.Size(166, 32);
            this.New_Exit_Button.TabIndex = 17;
            this.New_Exit_Button.Text = "Вийти з акаунту";
            this.New_Exit_Button.TextColor = System.Drawing.Color.Black;
            this.New_Exit_Button.UseVisualStyleBackColor = false;
            this.New_Exit_Button.Click += new System.EventHandler(this.New_Exit_Button_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = global::Repair_As_New.Properties.Resources.RAN_logos_transparent__1_;
            this.pictureBox1.Location = new System.Drawing.Point(224, 133);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(185, 185);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(891, 511);
            this.Controls.Add(this.panelChildForm);
            this.Controls.Add(this.panel_SideMenu);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(800, 450);
            this.Name = "Main_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RAN";
            this.panel_SideMenu.ResumeLayout(false);
            this.subpanelContacts.ResumeLayout(false);
            this.subpanelNews.ResumeLayout(false);
            this.subpanelPoslugu.ResumeLayout(false);
            this.PanelLogo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panelChildForm.ResumeLayout(false);
            this.panelChildForm.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_SideMenu;
        private System.Windows.Forms.Panel PanelLogo;
        private System.Windows.Forms.Panel panelChildForm;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel subpanelContacts;
        private System.Windows.Forms.Button subbuttonContacts3;
        private System.Windows.Forms.Button subbuttonContacts2;
        private System.Windows.Forms.Button subbuttonContacts1;
        private System.Windows.Forms.Button buttonContacts;
        private System.Windows.Forms.Button buttonOrders;
        private System.Windows.Forms.Button buttonCost;
        private System.Windows.Forms.Panel subpanelNews;
        private System.Windows.Forms.Button subbuttonNews3;
        private System.Windows.Forms.Button subbuttonNews2;
        private System.Windows.Forms.Button buttonNews;
        private System.Windows.Forms.Panel subpanelPoslugu;
        private System.Windows.Forms.Button subbuttonPoslugu6;
        private System.Windows.Forms.Button subbuttonPoslugu5;
        private System.Windows.Forms.Button subbuttonPoslugu4;
        private System.Windows.Forms.Button subbuttonPoslugu3;
        private System.Windows.Forms.Button subbuttonPoslugu2;
        private System.Windows.Forms.Button subbuttonPoslugu1;
        private System.Windows.Forms.Button buttonPoslugu;
        private System.Windows.Forms.PictureBox pictureBox2;
        private RoundButton New_Exit_Button;
    }
}