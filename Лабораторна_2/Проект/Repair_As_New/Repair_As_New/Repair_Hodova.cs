﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;


namespace Repair_As_New
{
    public partial class Repair_Hodova : Form
    {
        private Socket senderSocket;
        private String loggedUserName;
        private DateTime now;
        public Repair_Hodova(Socket senderSocket, String loggedUserName, DateTime now)
        {
            this.senderSocket = senderSocket;
            this.loggedUserName = loggedUserName;
            this.now = now;
            InitializeComponent();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Repair_Hodova_Load(object sender, EventArgs e)
        {

        }

        private void roundButton1_Click(object sender, EventArgs e)
        {
            Order_Form obj1 = new Order_Form(senderSocket, loggedUserName, now);
            obj1.Show();
            this.Hide();
        }
    }
}
