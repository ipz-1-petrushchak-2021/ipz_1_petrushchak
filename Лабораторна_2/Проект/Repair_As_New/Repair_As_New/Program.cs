﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Text;
using System.Text.RegularExpressions;

namespace Repair_As_New
{
   
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            DateTime now = DateTime.Now;
            //Application.Run(new Registration_Form());
            //Application.Run(new Main_Form());

            if (!Directory.Exists("log"))
            {
                Directory.CreateDirectory("log");
            }

            Socket sender = null;

            DialogResult dr2 = DialogResult.Retry;

            while (dr2 == DialogResult.Retry)
            {
                sender = StartClient2(sender, now);
                if (sender != null)
                {
                    Application.Run(new Login_Form(sender, now));
                    Repair_As_New.Program.sendData(sender, "closed", now);
                }
                sender = null;
                break;

            }
            Application.Exit();
        }
        public static Socket StartClient2(Socket sender, DateTime now)
        {
            while (sender == null)
            {                                                           //ipconfig /all
                sender = StartClient(now, "127.0.0.1", "8888");  //("127.0.0.1"), 8888));
                if (sender != null)
                {
                    return sender;
                }
                else
                {
                    DialogResult dr = MessageBox.Show("Try to start client again ?", "Server not found", MessageBoxButtons.RetryCancel);

                    writeToFile(now, "Server didn't start");

                    if (dr != DialogResult.Retry)
                        return null;
                }
            }
            return sender;
        }
        //-----------------Email correction check---------------------------
       /*
        public static bool IsValid(string emailaddress)
        {
            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");

            Match match = regex.Match(emailaddress);

            if (match.Success)
                return true;
            else
                return false;
        }
        */
        //-------------------------------------------------------------------
        //-------------------Writing the log data to file-------------------
        public static void writeToFile(DateTime time, String str)
        {
            DateTime currentTime = DateTime.Now;
            using (StreamWriter sw = File.AppendText($"log\\{time.Day}_{time.Month}_{time.Year}--{time.Hour}_{time.Minute}_{time.Second}.log"))
            {
                sw.WriteLine($"{currentTime} : {str} ");
            }
        }
        //-------------------------------------------------------------------
        //----------------------Sending data throught sockets to server-----
        public static void sendData(Socket sender, String str, DateTime now)
        {
            try
            {
                byte[] msg = Encoding.UTF8.GetBytes($"{str}<EOF>");
                sender.Send(msg);
                writeToFile(now, $"Send: {str}");
            }
            catch (Exception e)
            {

            }
        }
        //------------------------------------------------------------------
        //----------Receiving data from server sockets----------------------
        public static String receiveData(Socket sender, DateTime now)
        {
            try
            {
                byte[] bytes = new byte[16384];
                int bytesRec = sender.Receive(bytes);
                String receivedMessage = $"{Encoding.UTF8.GetString(bytes, 0, bytesRec)}";
                writeToFile(now, $"Received: {receivedMessage}");
                return receivedMessage;
            }
            catch (Exception e)
            {
                MessageBox.Show("Server disconnected");
                return "close";
            }
        }
        //----------------------------------------------------------------
        //-------------------starting socket connection-------------------
        public static Socket StartClient(DateTime now, String ip, String Port)
        {
            byte[] bytes = new byte[1024];

            Socket sender = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                sender.Connect(IPAddress.Parse(ip), Convert.ToInt32(Port));

                writeToFile(now, $"Socket connected to {sender.RemoteEndPoint.ToString()}");
            }
            catch (Exception e)
            {
                writeToFile(now, e.ToString());
                return null;
            }

            return sender;
        }
        //-------------------------------------------------------------------
    }
}
