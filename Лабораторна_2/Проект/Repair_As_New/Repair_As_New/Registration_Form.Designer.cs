﻿
namespace Repair_As_New
{
    partial class Registration_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.roundButton1 = new Repair_As_New.RoundButton();
            this.label2 = new System.Windows.Forms.Label();
            this.New_Exit_Button = new Repair_As_New.RoundButton();
            this.Phone_Number_Field = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.Login_Registation_Field = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.passwordFIELD2 = new System.Windows.Forms.TextBox();
            this.Email_Field = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.passwordFIELD = new System.Windows.Forms.TextBox();
            this.loginFIELD = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Phone_Number_Field)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Login_Registation_Field)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(202)))), ((int)(((byte)(240)))), ((int)(((byte)(248)))));
            this.panel1.Controls.Add(this.roundButton1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.New_Exit_Button);
            this.panel1.Controls.Add(this.Phone_Number_Field);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.Login_Registation_Field);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.passwordFIELD2);
            this.panel1.Controls.Add(this.Email_Field);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.passwordFIELD);
            this.panel1.Controls.Add(this.loginFIELD);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(800, 450);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // roundButton1
            // 
            this.roundButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(62)))), ((int)(((byte)(138)))));
            this.roundButton1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(62)))), ((int)(((byte)(138)))));
            this.roundButton1.BackgroundImage = global::Repair_As_New.Properties.Resources._197211_okay_ok_check_meanicons_icon;
            this.roundButton1.BorderColor = System.Drawing.Color.PaleVioletRed;
            this.roundButton1.BorderRadius = 25;
            this.roundButton1.BorderSize = 0;
            this.roundButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.roundButton1.FlatAppearance.BorderSize = 0;
            this.roundButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.roundButton1.ForeColor = System.Drawing.Color.White;
            this.roundButton1.Location = new System.Drawing.Point(394, 367);
            this.roundButton1.Name = "roundButton1";
            this.roundButton1.Size = new System.Drawing.Size(49, 48);
            this.roundButton1.TabIndex = 18;
            this.roundButton1.TextColor = System.Drawing.Color.White;
            this.roundButton1.UseVisualStyleBackColor = false;
            this.roundButton1.Click += new System.EventHandler(this.roundButton1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Arial Narrow", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(346, 418);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 23);
            this.label2.TabIndex = 17;
            this.label2.Text = "Вже маєте акаунт?";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // New_Exit_Button
            // 
            this.New_Exit_Button.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(62)))), ((int)(((byte)(138)))));
            this.New_Exit_Button.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(62)))), ((int)(((byte)(138)))));
            this.New_Exit_Button.BorderColor = System.Drawing.Color.PaleVioletRed;
            this.New_Exit_Button.BorderRadius = 10;
            this.New_Exit_Button.BorderSize = 0;
            this.New_Exit_Button.FlatAppearance.BorderSize = 0;
            this.New_Exit_Button.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.New_Exit_Button.Font = new System.Drawing.Font("Arial Narrow", 15.25F, System.Drawing.FontStyle.Bold);
            this.New_Exit_Button.ForeColor = System.Drawing.Color.White;
            this.New_Exit_Button.Location = new System.Drawing.Point(703, 406);
            this.New_Exit_Button.Name = "New_Exit_Button";
            this.New_Exit_Button.Size = new System.Drawing.Size(85, 32);
            this.New_Exit_Button.TabIndex = 16;
            this.New_Exit_Button.Text = "Вихід";
            this.New_Exit_Button.TextColor = System.Drawing.Color.White;
            this.New_Exit_Button.UseVisualStyleBackColor = false;
            this.New_Exit_Button.Click += new System.EventHandler(this.New_Exit_Button_Click);
            // 
            // Phone_Number_Field
            // 
            this.Phone_Number_Field.Image = global::Repair_As_New.Properties.Resources._3669371_contact_ic_phone_icon;
            this.Phone_Number_Field.Location = new System.Drawing.Point(210, 133);
            this.Phone_Number_Field.Name = "Phone_Number_Field";
            this.Phone_Number_Field.Size = new System.Drawing.Size(44, 44);
            this.Phone_Number_Field.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Phone_Number_Field.TabIndex = 14;
            this.Phone_Number_Field.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Repair_As_New.Properties.Resources._211660_email_icon;
            this.pictureBox4.Location = new System.Drawing.Point(207, 69);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(47, 46);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 13;
            this.pictureBox4.TabStop = false;
            // 
            // Login_Registation_Field
            // 
            this.Login_Registation_Field.Image = global::Repair_As_New.Properties.Resources._2203549_admin_avatar_human_login_user_icon__1_;
            this.Login_Registation_Field.Location = new System.Drawing.Point(208, 199);
            this.Login_Registation_Field.Name = "Login_Registation_Field";
            this.Login_Registation_Field.Size = new System.Drawing.Size(44, 44);
            this.Login_Registation_Field.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Login_Registation_Field.TabIndex = 12;
            this.Login_Registation_Field.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Repair_As_New.Properties.Resources._928417_access_lock_password_protect_safety_icon__1_;
            this.pictureBox1.Location = new System.Drawing.Point(208, 264);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(44, 44);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // passwordFIELD2
            // 
            this.passwordFIELD2.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.passwordFIELD2.Location = new System.Drawing.Point(285, 331);
            this.passwordFIELD2.Name = "passwordFIELD2";
            this.passwordFIELD2.Size = new System.Drawing.Size(269, 30);
            this.passwordFIELD2.TabIndex = 10;
            this.passwordFIELD2.UseSystemPasswordChar = true;
            this.passwordFIELD2.TextChanged += new System.EventHandler(this.passwordFIELD2_TextChanged);
            this.passwordFIELD2.Enter += new System.EventHandler(this.passwordFIELD2_Enter);
            this.passwordFIELD2.Leave += new System.EventHandler(this.passwordFIELD2_Leave);
            // 
            // Email_Field
            // 
            this.Email_Field.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Email_Field.Location = new System.Drawing.Point(285, 75);
            this.Email_Field.Multiline = true;
            this.Email_Field.Name = "Email_Field";
            this.Email_Field.Size = new System.Drawing.Size(269, 34);
            this.Email_Field.TabIndex = 9;
            this.Email_Field.TextChanged += new System.EventHandler(this.Email_Field_TextChanged);
            this.Email_Field.Enter += new System.EventHandler(this.Email_Field_Enter);
            this.Email_Field.Leave += new System.EventHandler(this.Email_Field_Leave);
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(285, 139);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(269, 34);
            this.textBox1.TabIndex = 8;
            this.textBox1.Enter += new System.EventHandler(this.textBox1_Enter);
            this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // passwordFIELD
            // 
            this.passwordFIELD.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.passwordFIELD.Location = new System.Drawing.Point(285, 272);
            this.passwordFIELD.Name = "passwordFIELD";
            this.passwordFIELD.Size = new System.Drawing.Size(269, 30);
            this.passwordFIELD.TabIndex = 7;
            this.passwordFIELD.UseSystemPasswordChar = true;
            this.passwordFIELD.TextChanged += new System.EventHandler(this.passwordFIELD_TextChanged);
            this.passwordFIELD.Enter += new System.EventHandler(this.passwordFIELD_Enter);
            this.passwordFIELD.Leave += new System.EventHandler(this.passwordFIELD_Leave);
            // 
            // loginFIELD
            // 
            this.loginFIELD.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.loginFIELD.Location = new System.Drawing.Point(285, 205);
            this.loginFIELD.Multiline = true;
            this.loginFIELD.Name = "loginFIELD";
            this.loginFIELD.Size = new System.Drawing.Size(269, 34);
            this.loginFIELD.TabIndex = 6;
            this.loginFIELD.Enter += new System.EventHandler(this.loginFIELD_Enter);
            this.loginFIELD.Leave += new System.EventHandler(this.loginFIELD_Leave);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(62)))), ((int)(((byte)(138)))));
            this.panel2.Controls.Add(this.pictureBox3);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(800, 61);
            this.panel2.TabIndex = 3;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Image = global::Repair_As_New.Properties.Resources._4115235_exit_logout_sign_out_icon;
            this.pictureBox3.Location = new System.Drawing.Point(748, 6);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(44, 44);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Arial Narrow", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(800, 61);
            this.label1.TabIndex = 1;
            this.label1.Text = "Реєстрація";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Registration_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Registration_Form";
            this.Text = "Registration_Form";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Phone_Number_Field)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Login_Registation_Field)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.TextBox passwordFIELD2;
        private System.Windows.Forms.TextBox Email_Field;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox passwordFIELD;
        private System.Windows.Forms.TextBox loginFIELD;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox Login_Registation_Field;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox Phone_Number_Field;
        private RoundButton New_Exit_Button;
        private System.Windows.Forms.Label label2;
        private RoundButton roundButton1;
    }
}