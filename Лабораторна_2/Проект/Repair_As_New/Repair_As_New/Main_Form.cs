﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;
using System.Net.Sockets;

namespace Repair_As_New
{
    public partial class Main_Form : Form
    {
        private Socket senderSocket;
        private String loggedUserName;
        private DateTime now;

        public Main_Form(Socket senderSocket, String loggedUserName, DateTime now)
        {
            this.senderSocket = senderSocket;
            this.loggedUserName = loggedUserName;
            this.now = now;
            InitializeComponent();
            myDesign();
        }

        

        private void panel_SideMenu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void myDesign()
        {
            subpanelPoslugu.Visible = false;
            subpanelNews.Visible = false;
            subpanelContacts.Visible = false;
        }

   

        private void offSubMenu()
        {
            if (subpanelPoslugu.Visible == true)
                subpanelPoslugu.Visible = false;
            if (subpanelNews.Visible == true)
                subpanelNews.Visible = false;
            if (subpanelContacts.Visible == true)
                subpanelContacts.Visible = false;
        }
        private void onSubMenu(Panel subMenu)
        {
            if (subMenu.Visible == false)
            {
                offSubMenu();
                subMenu.Visible = true;
            }
            else
            {
                subMenu.Visible = false;
            }
                
        }
        #region Poslugu
        private void buttonPoslugu_Click(object sender, EventArgs e)
        {
            onSubMenu(subpanelPoslugu);
        }

        private void subbuttonPoslugu1_Click(object sender, EventArgs e)
        {
            openChildForms(new Poslugu_Diagnostic());
            //some code
            offSubMenu();
        }

        private void subbuttonPoslugu2_Click(object sender, EventArgs e)
        {
            
            //openChildForms(new Test_Form());
            //some code
            offSubMenu();
        }

        private void subbuttonPoslugu3_Click(object sender, EventArgs e)
        {
            openChildForms(new Repair_Fuel_Aparature(senderSocket, loggedUserName, now));
            offSubMenu();
        }

        private void subbuttonPoslugu4_Click(object sender, EventArgs e)
        {
            openChildForms(new Repair_Hodova(senderSocket, loggedUserName, now));
            //some code
            offSubMenu();
        }

        private void subbuttonPoslugu5_Click(object sender, EventArgs e)
        {
            openChildForms(new Repair_Engine(senderSocket, loggedUserName, now));
            //some code
            offSubMenu();
        }

        private void subbuttonPoslugu6_Click(object sender, EventArgs e)
        {
            openChildForms(new Repair_KPP(senderSocket, loggedUserName, now));
            //some code
            offSubMenu();
        }

        private void subbuttonPoslugu7_Click(object sender, EventArgs e)
        {
            //some code
            offSubMenu();
        }
#endregion
        #region News
        private void buttonNews_Click(object sender, EventArgs e)
        {
            onSubMenu(subpanelNews);
        }

        private void subbuttonNews1_Click(object sender, EventArgs e)
        {
           
            //some code
            offSubMenu();
        }

        private void subbuttonNews2_Click(object sender, EventArgs e)
        {
            openChildForms(new news_Company());
            //some code
            offSubMenu();
        }

        private void subbuttonNews3_Click(object sender, EventArgs e)
        {
            openChildForms(new news_Sto());
            //some code
            offSubMenu();
        }
#endregion
        #region Contacts
        private void buttonContacts_Click(object sender, EventArgs e)
        {
            onSubMenu(subpanelContacts);
        }

        private void subbuttonContacts1_Click(object sender, EventArgs e)
        {
            openChildForms(new Hot_Line_Form());
            //some code
            offSubMenu();
        }

        private void subbuttonContacts2_Click(object sender, EventArgs e)
        {
            openChildForms(new Post_Form());
            //some code
            offSubMenu();
        }

        private void subbuttonContacts3_Click(object sender, EventArgs e)
        {
            openChildForms(new Main_office());
            //some code
            offSubMenu();
        }
        #endregion

        private Form activeForms = null;
        private void openChildForms(Form childForm)
        {
            if (activeForms != null)
                activeForms.Close();
            activeForms = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelChildForm.Controls.Add(childForm);
            panelChildForm.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();


        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
 
        }

        private void subbuttonPoslugu1_Click_1(object sender, EventArgs e)
        {
            openChildForms(new Poslugu_Diagnostic());
            offSubMenu();
        }

        private void buttonCost_Click(object sender, EventArgs e)
        {
            openChildForms(new Prices_Form());
            
        }

        private void panelChildForm_Paint(object sender, PaintEventArgs e)
        {

        }

        private void subbuttonPoslugu2_Click_1(object sender, EventArgs e)
        {
            openChildForms(new Repair_Rulova_Reyka(senderSocket, loggedUserName, now));
            offSubMenu();
        }

        private void buttonOrders_Click(object sender, EventArgs e)
        {
            openChildForms(new Order_Form(senderSocket, loggedUserName, now));
            offSubMenu();
        }

        private void New_Exit_Button_Click(object sender, EventArgs e)
        {
            this.Hide();
            Repair_As_New.Program.sendData(senderSocket, $"log out '{loggedUserName}' ", now);
            Login_Form loging_menu = new Login_Form(this.senderSocket, now);
            loging_menu.Show();
        }

        private void subpanelPoslugu_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
