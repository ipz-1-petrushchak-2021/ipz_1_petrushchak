﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;


namespace Repair_As_New
{
    public partial class Login_Form : Form
    {
        private Socket senderSocket;
        private DateTime now;
        public Login_Form(Socket senderSocket, DateTime now)
        {
            this.senderSocket = senderSocket;
            this.now = now;
            InitializeComponent();
            this.passwordFIELD.AutoSize = false;
            this.passwordFIELD.Size = new Size(this.passwordFIELD.Size.Width, 34);
        }
        //public Login_Form()
        //{
        //   InitializeComponent();
        //   this.passwordFIELD.AutoSize = false;
        //   this.passwordFIELD.Size = new Size(this.passwordFIELD.Size.Width,34);
        //
        //}

        private void label1_Click(object sender, EventArgs e)
        {

        }



        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        Point lastPoint;
        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void label1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void label1_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Repair_As_New.Program.sendData(senderSocket, $"closed", now);
            this.Close();
            Application.Exit();
        }

        private void New_Exit_Button_Click(object sender, EventArgs e)
        {
            Repair_As_New.Program.sendData(senderSocket, $"closed", now);
            this.Close();
            Application.Exit();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Registration_Form logging = new Registration_Form(this.senderSocket, this.now);
            logging.Show();
        }
        //-----------------Login button click---------------------
        private void roundButton1_Click(object sender, EventArgs e)
        {
            if (loginFIELD.Text != "")
            {
                bool passVerification = false;

                Repair_As_New.Program.sendData(senderSocket, $"select count(*) from clients where user_login = '{this.loginFIELD.Text}' and user_password = '{this.passwordFIELD.Text}'", this.now);
                String v1 = Repair_As_New.Program.receiveData(senderSocket, now);

                if (v1.Contains("pass"))
                {
                    passVerification = true;
                }

                if (passVerification)
                {
                    this.Hide();
                    Main_Form main_menu = new Main_Form(senderSocket, Repair_As_New.Program.receiveData(senderSocket, now), this.now);
                    main_menu.Show();
                }
                else if (v1.Contains("is already logged in"))
                {
                    MessageBox.Show("This user is already logged in!");
                }
                else if (v1.Contains("fail"))
                {
                    MessageBox.Show("Wrong login or password!");
                }

            }
            else
            {
                MessageBox.Show("Enter your actual login");     //("Enter a valid email");
                loginFIELD.Text = "";
            }
        }

        private void loginFIELD_TextChanged(object sender, EventArgs e)
        {

        }
        //--------------------------------------------------------
    }
}
