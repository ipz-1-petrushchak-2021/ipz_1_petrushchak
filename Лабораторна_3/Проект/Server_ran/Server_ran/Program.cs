﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Data.SqlClient;
using System.IO;



namespace Server_ran
{
   

    class Program
    {
        //-------------------------Creatin Log file-------------------------------
        public static void writeToFile(DateTime time, String str)
        {
            DateTime currentTime = DateTime.Now;
            using (StreamWriter sw = File.AppendText($"log\\{time.Day}_{time.Month}_{time.Year}--{time.Hour}_{time.Minute}_{time.Second}.log"))
            {
                sw.WriteLine($"{currentTime} : {str} ");
            }
        }
        //-----------------------------------------------------------------------


        //-----------------------Initializing sokets-----------------------------
        static DateTime now = DateTime.Now;
        private static readonly Socket listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static readonly List<Socket> heandler = new List<Socket>();
        private static readonly List<string> onlineusers = new List<string>(10) { null, null, null, null, null, null, null, null, null, null };
        private const int BUFFER_SIZE = 2048;
        private static readonly byte[] buffer = new byte[BUFFER_SIZE];
        //------------------------------------------------------------------------


        //-------------------------Receive procedure starting--------------------
        private static void ReceiveCallback(IAsyncResult AR)
        {
            Socket current = (Socket)AR.AsyncState;
            int received;
            try
            {
                received = current.EndReceive(AR);
            }
            catch (SocketException ex)
            {
                Console.WriteLine($"Client {current.RemoteEndPoint.ToString()}  forcefully disconnected");
                // Don't shutdown because the socket may be disposed and its disconnected anyway.
                int i = heandler.IndexOf(current);
                current.Close();
                heandler.Remove(current);
                onlineusers.RemoveAt(i);
                return;
            }
            //-------------------Recieve procedure ending--------------------------


            //-------------------SQL connecting-----------------------------------
            SqlConnection _sql;
            string connectionString = "Server=DESKTOP-Q4M52L7; Database= RAN; Integrated Security = SSPI;";
            _sql = new SqlConnection(connectionString);

            _sql.Open();

            byte[] recBuf = new byte[received];
            //-------------------------------------------------------------------


            try
            {
                Array.Copy(buffer, recBuf, received - 5);
            }
            catch (System.ArgumentOutOfRangeException exep)
            {
                // Don't shutdown because the socket may be disposed and its disconnected anyway.
                current.Shutdown(SocketShutdown.Both);
                current.Close();
                heandler.Remove(current);
                return;
            }

            string text = Encoding.UTF8.GetString(recBuf); 
            Console.WriteLine("Received massage: " + text);
            writeToFile(now, $"Received: {text}");


            //===========================================================================
            //===========================================================================
            //------------------------------------------------sql_start------------------
            //                  SQL closing aplication and his sokets
            if (text.Contains("closed"))
            {
                Console.WriteLine($"Client {current.RemoteEndPoint.ToString()} accurately disconnected");
                writeToFile(now, $"Client {current.RemoteEndPoint.ToString()} accurately disconnected");
                current.Shutdown(SocketShutdown.Both);
                current.Close();
                heandler.Remove(current);
                _sql.Close();
                return;
            }
            //                      SQL     REGISTARTION
            else if (text.Contains("user_registration"))
            {
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();

                command = new SqlCommand(text, _sql);

                adapter.InsertCommand = new SqlCommand(text, _sql);
                try
                {
                    adapter.InsertCommand.ExecuteNonQuery();
                    sendData(current, "pass", now);
                    Console.WriteLine("pass");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Wrong command - {0}", text);
                    sendData(current, "fail", now);
                    Console.WriteLine("fail");
                }

                command.Dispose();
            }
            //              SQL USER SERVISE REGISTRATION
            else if (text.Contains("records_registration"))
            {
                SqlCommand command;
                SqlDataAdapter adapter = new SqlDataAdapter();

                command = new SqlCommand(text, _sql);

                adapter.InsertCommand = new SqlCommand(text, _sql);
                try
                {
                    adapter.InsertCommand.ExecuteNonQuery();
                    sendData(current, "pass", now);
                    Console.WriteLine("pass");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Wrong command - {0}", text);
                    sendData(current, "fail", now);
                    Console.WriteLine("fail");
                }

                command.Dispose();
            }
            //              SQL    LOGIN
            else if (text.Contains("count(*)"))
            {
                SqlCommand sqlCommand = new SqlCommand(text, _sql);
                int count = (int)sqlCommand.ExecuteScalar();

                if (count == 1)
                {
                    SqlDataReader dataReader;
                    String Output = "";

                    SqlCommand command = new SqlCommand(text.Replace("count(*)", "clients.user_login, clients.email"), _sql);
                    dataReader = command.ExecuteReader();

                    while (dataReader.Read())
                    {
                        Output = dataReader.GetValue(0) + " " + dataReader.GetValue(1);
                    }
                    dataReader.Close();
                    command.Dispose();
                    if (!onlineusers.Contains(Output))
                    {
                        sendData(current, "pass", now);
                        Console.WriteLine("pass");
                        writeToFile(now, "pass");

                        sendData(current, Output, now);
                        Console.WriteLine(Output);
                        int d = heandler.IndexOf(current);
                        onlineusers.Insert(d, Output);
                    }
                    else
                    {
                        sendData(current, $"{Output} is already logged in", now);
                        Console.WriteLine($"{Output} is already logged in");
                    }
                }
                else
                {
                    sendData(current, "fail", now);
                    Console.WriteLine("fail");
                }
                sqlCommand.Dispose();
            }
            //       SQL          ARE USER  ONLINE ? 
            else if (text.Contains("onlineuser"))
            {
                SqlDataReader dataReader;
                String Output = "";
                SqlCommand command = new SqlCommand(text.Replace("add onlineuser", "select user_login, email from clients where CONCAT(clients.user_login, ' ', clients.email) ="), _sql);
                dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Output = dataReader.GetValue(0) + " " + dataReader.GetValue(1);
                }
                int d = heandler.IndexOf(current);
                onlineusers.Insert(d, Output);
                dataReader.Close();
                command.Dispose();
            }
            //                  SQL      CHECK LOGIN 
            else if (text.Contains("check"))
            {

                SqlCommand sqlCommand = new SqlCommand(text.Replace("check", "select count(*) from clients where"), _sql);
                int count = (int)sqlCommand.ExecuteScalar();
                if (count == 0)
                {
                    sendData(current, "pass", now);
                    Console.WriteLine("pass");
                    writeToFile(now, "pass");
                }
                else
                {
                    sendData(current, "fail", now);
                    Console.WriteLine("fail");
                }
            }
            //              SQL    CHECK TIME FOR SERVICE REGISTRATION
            else if (text.Contains("busy"))
            {

                SqlCommand sqlCommand = new SqlCommand(text.Replace("busy", "select count(*) from service_records where"), _sql);
                int count = (int)sqlCommand.ExecuteScalar();
                if (count == 0)
                {
                    sendData(current, "pass", now);
                    Console.WriteLine("pass");
                    writeToFile(now, "pass");
                }
                else
                {
                    sendData(current, "fail", now);
                    Console.WriteLine("fail");
                }
            }        //1
                     //           SQL        taking cities from database
            else if (text.Contains("select city_name from cities")) 
            {
                SqlCommand sqlCommand = new SqlCommand(text, _sql);
                String Output = "";
                SqlDataReader dataReader;
                dataReader = sqlCommand.ExecuteReader();


                while (dataReader.Read())
                {
                    Output += dataReader.GetValue(0) + "?";
                }
                dataReader.Close();

                sendData(current, Output, now);
                Console.WriteLine("cities loaded");
                sqlCommand.Dispose();
            }
            else if (text.Contains("select service_name from ran_services"))
            {

                SqlCommand sqlCommand = new SqlCommand(text, _sql);
                String Output = "";
                SqlDataReader dataReader;
                dataReader = sqlCommand.ExecuteReader();

                while (dataReader.Read())
                {
                    Output += dataReader.GetValue(0) + "?";
                }
                dataReader.Close();

                sendData(current, Output, now);
                Console.WriteLine("services loaded");
                sqlCommand.Dispose();
            }
            //===========================================================================
            //===========================================================================
            //-------------stop recieving callback--------------------------------------
            _sql.Close();
            current.BeginReceive(buffer, 0, BUFFER_SIZE, SocketFlags.None, ReceiveCallback, current);


        }
        //-----------------sending data throught socket to server----------------------
        public static void sendData(Socket handler, string data, DateTime now)
        {
            byte[] msg = Encoding.UTF8.GetBytes(data);
            handler.Send(msg);
            writeToFile(now, $"Sent: {data}");
        }
        //------------------------------------------------------------------------------
        //------------------closing sokets (like turning off light before going to bed---
        private static void CloseAllSockets()
        {
            foreach (Socket socket in heandler)
            {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }

            listener.Close();
        }
        //---------------------darkness................................................
        private static void AcceptCallback(IAsyncResult AR)
        {
            Socket socket;

            try
            {
                socket = listener.EndAccept(AR);
            }
            catch (ObjectDisposedException ex) // I cannot seem to avoid this (on exit when properly closing sockets)
            {
                return;
            }

            heandler.Add(socket);
            socket.BeginReceive(buffer, 0, BUFFER_SIZE, SocketFlags.None, ReceiveCallback, socket);
            Console.WriteLine($"Client {socket.RemoteEndPoint.ToString()}  connected, waiting for request...");
            listener.BeginAccept(AcceptCallback, null);
        }
        private static void SetupServer()
        {
            Console.WriteLine("Setting up server...");                                  //ipconfig /all
            listener.Bind(new IPEndPoint(IPAddress.Parse("192.168.31.229"), 8888));  //("127.0.0.1"), 8888));
            listener.Listen(5);
            listener.BeginAccept(AcceptCallback, null);
            Console.WriteLine("Server setup complete");
        }
        static void Main(string[] args)
        {
            Console.Title = "Server";
            if (!Directory.Exists("log"))
            {
                Directory.CreateDirectory("log");
            }

            SetupServer();
            Console.ReadLine(); // When we press enter close everything
            CloseAllSockets();
        }

    }
}
